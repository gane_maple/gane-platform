package com.gane.auth.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description TODO
 * @Date 2020/5/10 9:01
 * @Created by 王弘博
 */
@Data
public class CurrentUser implements Serializable {

    private Integer userId;

    private Integer departmentId;

    private String loginName;

    private List<String> authorities;
}
