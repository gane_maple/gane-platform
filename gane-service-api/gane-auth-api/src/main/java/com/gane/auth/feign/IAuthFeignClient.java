package com.gane.auth.feign;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Description TODO
 * @Date 2020/4/27 13:36
 * @Created by 王弘博
 */
@FeignClient(value = "gane-auth")
public interface IAuthFeignClient {

}
