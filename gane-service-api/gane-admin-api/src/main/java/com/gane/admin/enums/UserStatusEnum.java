package com.gane.admin.enums;

/**
 * @Description TODO
 * @Date 2020/4/27 17:45
 * @Created by 王弘博
 */
public enum UserStatusEnum {

    NORMAL("0", "正常"),
    CLOSED("1", "停用"),
    ;

    private final String code;

    private final String desc;

    UserStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }
}
