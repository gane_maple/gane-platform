package com.gane.admin.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description UserDTO
 * @Date 2020/4/23 23:20
 * @Created by 王弘博
 */
@Data
public class UserDTO implements Serializable {

    private Integer id;

    /**
     * 登录账户
     */
    private String loginName;

    /**
     * 昵称 花名
     */
    private String nickName;

    /**
     * 管理员类型（00系统管理员）
     */
    private String userType;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 密码盐值
     */
    private String salt;

    /**
     * 帐号状态（0正常 1停用）
     */
    private String status;

    /**
     * 角色列表
     */
    private List<RoleDTO> roleDTOS = new ArrayList<>();

}
