package com.gane.admin.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description TODO
 * @Date 2020/4/29 19:55
 * @Created by 王弘博
 */
@Data
public class RoleDTO implements Serializable {

    /**
     * 主鍵ID
     */
    private Integer id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色标识
     */
    private String roleKey;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 数据范围（1：全部数据权限 2：自定数据权限）
     */
    private String dataScope;

    /**
     * 部门状态（0正常 1停用）
     */
    private String status;

    /**
     * 该角色所拥有的权限列表
     */
    private List<PermssionDTO> permssionDTOS = new ArrayList<>();
}
