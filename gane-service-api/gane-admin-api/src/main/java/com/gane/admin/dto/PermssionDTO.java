package com.gane.admin.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description TODO
 * @Date 2020/4/29 20:34
 * @Created by 王弘博
 */
@Data
public class PermssionDTO implements Serializable {

    /**
     * 主鍵ID
     */
    private Integer id;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单标识
     */
    private String menuKey;

    /**
     * 祖级列表
     */
    private String parentId;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    private String menuType;

    /**
     * 权限标识
     */
    private String perms;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 重定向
     */
    private String redirect;
}
