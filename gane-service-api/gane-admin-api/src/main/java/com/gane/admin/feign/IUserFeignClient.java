package com.gane.admin.feign;

import com.gane.admin.dto.PermssionDTO;
import com.gane.admin.dto.RoleDTO;
import com.gane.admin.dto.UserDTO;
import com.gane.common.template.result.ServiceResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Description TODO
 * @Date 2020/4/27 16:37
 * @Created by 王弘博
 */
@FeignClient(value = "gane-admin")
public interface IUserFeignClient {

    @GetMapping("/queryByLoginName")
    ServiceResult<UserDTO> queryByLoginName(@RequestParam(value = "loginName") String loginName);

    @GetMapping("/queryRolesByUserId")
    ServiceResult<List<RoleDTO>> queryRolesByUserId(@RequestParam(value = "userId") Integer userId);

    @GetMapping("/queryPermsByRoleIds")
    ServiceResult<List<PermssionDTO>> queryPermsByRoleIds(@RequestParam(value = "roleIds") List<Integer> roleIds);
}
