package com.gane.common.template.service;

import com.gane.common.template.action.ServiceAction;
import com.gane.common.template.result.ServiceResult;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public interface ServiceInterceptor {

    <T> void intercept(ServiceAction<T> action, ServiceResult<T> serviceResult) throws Exception;
}
