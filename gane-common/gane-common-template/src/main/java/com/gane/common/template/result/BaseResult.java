package com.gane.common.template.result;

import com.gane.common.template.constant.BizErrorCode;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
@Data
public class BaseResult<T> implements Serializable {

    private boolean success = true;

    //private T data;

    private BizErrorCode resultCode = BizErrorCode.SUCCESS;

    public BaseResult() {
    }

    public BaseResult(T data) {
        //this.data = data;
        this.success = true;
        this.resultCode = BizErrorCode.SUCCESS;
    }

    /*@Data
    public class ErrorContext {
        private String errorCode;
        private String errorMsg;

        public ErrorContext(String errorCode, String errorMsg) {
            this.errorCode = errorCode;
            this.errorMsg = errorMsg;
        }
    }*/


}
