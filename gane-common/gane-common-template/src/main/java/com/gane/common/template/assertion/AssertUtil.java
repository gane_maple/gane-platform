package com.gane.common.template.assertion;

import com.gane.common.template.exception.AssertionException;
import com.gane.common.template.exception.AssertionResultCode;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Constructor;

/**
 * @Description TODO
 * @Date 2020/4/24 18:25
 * @Created by 王弘博
 */
public class AssertUtil {

    private static String exceptionClassName;

    private static Constructor constructor;

    public AssertUtil() {

    }

    public static void notNull(Object object, AssertionResultCode resultCode, Object... objs) {
        isTrue(object != null, resultCode, objs);
    }

    public static void isFalse(boolean expValue, AssertionResultCode resultCode, Object... objs) {
        isTrue(!expValue, resultCode, objs);
    }

    public static void isTrue(boolean expValue, AssertionResultCode resultCode, Object... objs) {

        if (!expValue) {

            AssertionException exception = null;
            String logString = getLogString(objs);
            String resultMsg = StringUtils.isBlank(logString) ? resultCode.getResultMsg() : logString;

            try {

                exception = (AssertionException) constructor.newInstance(resultMsg);

            } catch (Throwable t) {
                throw new IllegalArgumentException("AssertUtil has notbeen initiallized correctly![exceptionClassName="
                        + exceptionClassName + ",resultCode=" + resultCode + ",resultMsg=" + resultMsg + "]", t);
            }

            exception.setResultCode(resultCode);
            throw exception;
        }

    }

    private static void initConfig() {

        Class exceptionClassTmp = null;

        if (StringUtils.isBlank(exceptionClassName)) {
            throw new IllegalArgumentException("exceptionClassName has not set!");
        }

        try {

            exceptionClassTmp = Class.forName(exceptionClassName);

        } catch (Throwable e) {

            throw new IllegalArgumentException("loading exceptionClass failed![exceptionClassName=" + exceptionClassName + "]", e);
        }

        if (!AssertionException.class.isAssignableFrom(exceptionClassTmp)) {

            throw new IllegalArgumentException("illegal exceptionClass type,must be the subclass of AssertionException![exceptionClassName=" + exceptionClassName + "]");
        }

        Constructor constructorTmp = null;

        try {

            constructorTmp = exceptionClassTmp.getConstructor(String.class);

        } catch (Throwable t) {

            throw new IllegalArgumentException("constructor method not found![exceptionClassName=" + exceptionClassName + "]", t);
        }

        constructor = constructorTmp;
    }

    private static String getLogString(Object... objs) {

        StringBuilder log = new StringBuilder();

        Object[] arr = objs;

        int len = objs.length;

        for (int i = 0; i < len; i++) {
            Object o = arr[i];
            log.append(o);
        }

        return log.toString();
    }

    public void setExceptionClassName(String exceptionClassName) {
        AssertUtil.exceptionClassName = exceptionClassName;
        initConfig();
    }
}
