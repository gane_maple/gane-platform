package com.gane.common.template.filter;

import com.gane.common.template.action.Action;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public interface Filter {

    <V> V execute(Action<V> action, Object... args) throws Exception;

}
