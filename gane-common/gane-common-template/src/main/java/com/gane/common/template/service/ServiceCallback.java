package com.gane.common.template.service;

import com.gane.common.template.result.ServiceResult;

/**
 * @Description TODO
 * @Date 2020/4/24 16:31
 * @Created by 王弘博
 */
public abstract class ServiceCallback<T> {

    public void beforeService() {

    }

    public abstract ServiceResult<T> executeService();

    public void afterService(ServiceResult<T> result) throws Throwable {

    }

    public void sendEventLog(ServiceResult<T> result) {
        //发消息
    }
}
