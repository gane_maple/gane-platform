package com.gane.common.template.result;

import com.gane.common.template.constant.BizErrorCode;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description TODO
 * @Date 2020/4/24 12:04
 * @Created by 王弘博
 */
@Data
public class ServiceResult<T> extends BaseResult {

    private T resultObj;

    private Map<String, Object> extInfoMap;

    public static <T> ServiceResult<T> valueOfSuccess() {
        return valueOfSuccess(null);
    }

    public static <T> ServiceResult<T> valueOfSuccess(T value) {
        ServiceResult<T> result = new ServiceResult<>();
        result.setSuccess(true);
        result.resultObj = value;
        result.setResultCode(BizErrorCode.SUCCESS);
        return result;
    }

    public static <T> ServiceResult<T> valueOfFail() {
        return valueOfFail(null, BizErrorCode.SYSTEM_ERROR);
    }

    public static <T> ServiceResult<T> valueOfFail(BizErrorCode errorCode) {
        return valueOfFail(null, errorCode);
    }

    public static <T> ServiceResult<T> valueOfFail(T value, BizErrorCode errorCode) {
        ServiceResult<T> result = new ServiceResult<>();
        result.setSuccess(false);
        result.resultObj = value;
        result.setResultCode(errorCode);
        return result;
    }

    public void putExtInfo(String key, Object obj) {
        if (null == extInfoMap) {
            extInfoMap = new HashMap<>(3);
        }
        extInfoMap.put(key, obj);
    }

    public Object fetchExtInfoByKey(String key) {
        if (null == extInfoMap) {
            return null;
        }
        return extInfoMap.get(key);
    }

}
