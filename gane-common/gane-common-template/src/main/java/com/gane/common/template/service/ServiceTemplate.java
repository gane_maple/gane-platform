package com.gane.common.template.service;


import com.gane.common.template.action.Action;
import com.gane.common.template.action.ServiceAction;
import com.gane.common.template.filter.Filter;
import com.gane.common.template.pipeline.DefaultPipeline;
import com.gane.common.template.pipeline.Pipeline;
import com.gane.common.template.result.ServiceResult;

import java.util.List;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public class ServiceTemplate {

    private Pipeline pipeline = new DefaultPipeline();

    public ServiceTemplate setInterceptors(List<ServiceInterceptor> interceptors) {
        pipeline.clearFilters();
        return addInterceptors(interceptors);
    }

    public ServiceTemplate addInterceptors(List<ServiceInterceptor> interceptors) {
        if (interceptors != null) {
            interceptors.forEach(this::addInterceptor);
        }
        return this;
    }

    public ServiceTemplate addInterceptor(ServiceInterceptor interceptor) {
        pipeline.addFilter(new Filter() {
            @Override
            public <T> T execute(Action<T> action, Object... args) throws Exception {
                interceptor.intercept(result -> {
                    action.execute(result);
                }, (ServiceResult<T>) args[0]);
                return null;
            }
        });
        return this;
    }

    public <T> ServiceResult<T> safeExecute(ServiceAction<T> action) throws Exception {
        return safeExecute(action, null);
    }

    public <T> ServiceResult<T> safeExecute(ServiceAction<T> action, ExceptionHandler<T> handler) throws Exception {
        ServiceResult<T> serviceResult = new ServiceResult<>();
        try {
            pipeline.execute((args) -> {
                action.execute((ServiceResult<T>) args[0]);
                return null;
            }, serviceResult);
        } catch (Exception e) {
            if (handler != null && handler.handle(serviceResult, e)) {
                return serviceResult;
            }
            throw e;
        }
        return serviceResult;
    }

    public <T> ServiceResult<T> execute(ServiceAction<T> action) {
        return execute(action, null);
    }

    /**
     * @param action
     * @param handler
     * @return
     */
    public <T> ServiceResult<T> execute(ServiceAction<T> action, ExceptionHandler<T> handler) {
        try {
            return safeExecute(action, handler);
        } catch (Exception e) {
            throw (RuntimeException) e;
        }
    }

    public interface ExceptionHandler<T> {

        /**
         * 处理异常，服务提供方使用
         * 转换成result的状态码
         * 返回true表示异常已被处理；否则表示异常不可处理，继续抛出
         *
         * @param serviceResult
         * @param e
         * @return
         */
        boolean handle(ServiceResult<T> serviceResult, Exception e);

    }
}
