package com.gane.common.template.filter;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public abstract class AfterFilter extends AbstractFilter {

    @Override
    protected void beforeExecute(Object... args) throws Exception {

    }
}
