package com.gane.common.template.service;

import com.gane.common.template.result.ServiceResult;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description ServiceTemplateImpl
 * @Date 2020/4/24 16:15
 * @Created by 王弘博
 */
@Slf4j
public class ServiceTemplateImpl {

    /**
     * 服务模板的标准执行流程
     *
     * @param callback
     * @param <T>
     * @return
     */
    public static <T> ServiceResult<T> serviceProcess(ServiceCallback<T> callback) {

        ServiceResult<T> result = null;

        try {

            result = doServiceProcess(callback);

            return result;

        } finally {

            try {

                callback.afterService(result);

                callback.sendEventLog(result);

            } catch (Throwable e) {

                log.error("{}", e);
            }

        }
    }

    private static <T> ServiceResult<T> doServiceProcess(ServiceCallback<T> callback) {

        callback.beforeService();

        return callback.executeService();

    }
}
