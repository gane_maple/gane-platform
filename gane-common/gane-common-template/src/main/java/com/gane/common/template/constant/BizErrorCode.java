package com.gane.common.template.constant;

import com.gane.common.template.exception.AssertionResultCode;
import org.apache.commons.lang3.StringUtils;

/**
 * @Description TODO
 * @Date 2020/4/24 11:46
 * @Created by 王弘博
 */
public enum BizErrorCode implements ErrorLevels, ErrorTypes, AssertionResultCode {

    SUCCESS(INFO, SYSTEM, "000", "success"),

    PARAM_ILLEGA(ERROR, SYSTEM, "001", "request param illegal"),

    SYSTEM_ERROR(ERROR, SYSTEM, "002", "unknown system error"),

    USER_HAS_EXIST(ERROR, BIZ, "003", "user has exist"),

    USER_NOT_EXIST(ERROR, BIZ, "004", "user not exist"),

    USER_IS_CLOSED(ERROR, BIZ, "005", "user is closed"),

    USER_LOGIN_NAME_HAS_EXIST(ERROR, BIZ, "004", "user login name has exist"),

    USER_NICK_NAME_HAS_EXIST(ERROR, BIZ, "005", "user nick name has exist"),

    USER_EMAIL_HAS_EXIST(ERROR, BIZ, "006", "user email has exist"),

    USER_MOBILE_HAS_EXIST(ERROR, BIZ, "007", "user mobile has exist"),

    USERNAME_OR_PASSWORD_ERROR(ERROR, BIZ, "008", "username or password error"),

    UNAUTHORIZED(ERROR, BIZ, "009", "unauthorized"),

    DEPARTMENT_NOT_EXIST(ERROR, BIZ, "101", "department not exist"),

    DEPARTMENT_HAS_EXIST(ERROR, BIZ, "102", "department has exist"),

    ;

    private final String errorLevel;

    private final String errorType;

    private final String code;

    private final String description;

    BizErrorCode(String errorLevel, String errorType, String code, String description) {
        this.errorLevel = errorLevel;
        this.errorType = errorType;
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getResultCode() {
        return code;
    }

    @Override
    public String getResultMsg() {
        return description;
    }

    public static BizErrorCode getByDesc(String description) {

        if (StringUtils.isBlank(description)) {
            return null;
        }

        for (BizErrorCode errorCode : values()) {
            if (description.equals(errorCode.getResultMsg())) {
                return errorCode;
            }
        }
        return null;
    }
}
