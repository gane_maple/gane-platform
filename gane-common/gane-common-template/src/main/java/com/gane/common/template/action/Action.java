package com.gane.common.template.action;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public interface Action<V> {

    V execute(Object... args) throws Exception;
}
