package com.gane.common.template.constant;

/**
 * @Description TODO
 * @Date 2020/4/24 11:54
 * @Created by 王弘博
 */
public interface ErrorTypes {

    String SYSTEM = "0";
    String BIZ = "1";
    String THIRD_PARTY = "2";
}
