package com.gane.common.template.constant;

/**
 * @Description TODO
 * @Date 2020/4/24 11:54
 * @Created by 王弘博
 */
public interface ErrorLevels {

    String INFO = "1";
    String WARN = "3";
    String ERROR = "5";
    String FATAL = "7";
}
