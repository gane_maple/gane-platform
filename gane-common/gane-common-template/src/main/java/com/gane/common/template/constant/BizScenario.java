package com.gane.common.template.constant;

/**
 * @Description TODO
 * @Date 2020/4/24 11:17
 * @Created by 王弘博
 */
public enum BizScenario {

    COMMON("10000000","10001000","no event log's common scenario"),


    USER_LOGIN("11000000","11001000","user login"),

    USER_CREATE("11000001","11001001","user create"),

    USER_LOAD("11000002","11001002","user load"),



    DEPARTMENT_LOAD("12000000","12001000","department load"),

    DEPARTMENT_TREE("12000001","12001001","department tree"),

    DEPARTMENT_DELETE("12000002","12001002","department delete"),

    DEPARTMENT_CREATE("12000003","12001003","department create"),


    ;

    private final String eventCode;

    private final String pdEventCode;

    private final String description;

    BizScenario(String eventCode, String pdEventCode, String description) {
        this.eventCode = eventCode;
        this.pdEventCode = pdEventCode;
        this.description = description;
    }

    public String getEventCode() {
        return eventCode;
    }

    public String getPdEventCode() {
        return pdEventCode;
    }
}
