package com.gane.common.template.constant;

/**
 * @Description TODO
 * @Date 2020/5/10 11:45
 * @Created by 王弘博
 */
public interface Constants {

    public static final String PAGE_COUNT = "pageCount";

    public static final String TOTAL_COUNT = "totalCount";
}
