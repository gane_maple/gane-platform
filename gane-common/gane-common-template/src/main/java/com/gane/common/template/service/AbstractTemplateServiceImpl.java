package com.gane.common.template.service;

import com.gane.common.template.assertion.AssertUtil;
import com.gane.common.template.constant.BizErrorCode;
import com.gane.common.template.constant.BizScenario;
import com.gane.common.template.convert.RequestConvertorFactory;
import com.gane.common.template.exception.BizException;
import com.gane.common.template.request.BaseRequest;
import com.gane.common.template.result.BaseResult;
import com.gane.common.template.result.ServiceResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.Validator;

import javax.annotation.Resource;
import java.util.stream.Collectors;

/**
 * @Description TODO
 * @Date 2020/4/24 11:10
 * @Created by 王弘博
 */
@Slf4j
public class AbstractTemplateServiceImpl extends BaseTemplateServiceImpl {

    @Resource
    private Validator validator;

    protected <T> BaseResult bizProcess(BizScenario bizScenario, BaseRequest request,
                                        Class<? extends BaseResult> resultClazz, FacadeCallback<T> callback) {

        long startTime = System.currentTimeMillis();

        BaseResult result = new BaseResult();

        try {

            // 1、初始化上下文
            initContext(bizScenario);

            // 2、参数检查
            callback.checkParams(request);

            // 3、参数转换
            Object[] args = RequestConvertorFactory.convertor(request);

            // 4、业务操作回调
            ServiceResult<T> serviceResult = callback.execute(args);

            // 5、执行结果组装
            result = callback.assembleResult(serviceResult, resultClazz);

        } catch (BizException be) {

            // 处理业务异常
            log.error("{}", be);

            result.setSuccess(false);
            result.setResultCode(be.getErrorCode());

        } catch (Throwable t) {

            // 处理系统异常
            log.error("{}", t);

            result.setSuccess(false);
            result.setResultCode(BizErrorCode.SYSTEM_ERROR);

        } finally {

            long elapseTime = System.currentTimeMillis() - startTime;

            System.out.println("耗时：" + elapseTime + "ms");

            // 打印摘要日志

            // 清空上下文
            clearContext();

        }

        return result;

    }

    /**
     * 接口的回调
     *
     * @param <T>
     */
    public abstract class FacadeCallback<T> {

        /**
         * 基本的参数检查
         *
         * @param request
         */
        protected void checkParams(BaseRequest request) {

            BindException errors = new BindException(request, "request");

            validator.validate(request, errors);

            AssertUtil.isFalse(errors.hasErrors(), BizErrorCode.PARAM_ILLEGA,
                    errors.getAllErrors().stream().map(error -> error.getDefaultMessage()).collect(Collectors.toList()));
        }

        /**
         * 具体的业务执行
         *
         * @param args
         * @return
         */
        protected abstract ServiceResult<T> execute(Object... args);

        /**
         * 组装结果报文
         *
         * @param serviceResult
         * @param resultClazz
         * @param <T>
         * @return
         * @throws Throwable
         */
        protected <T> BaseResult assembleResult(ServiceResult<T> serviceResult,
                                                Class<? extends BaseResult> resultClazz) throws Throwable {
            BaseResult result = resultClazz.newInstance();
            result.setSuccess(serviceResult.isSuccess());
            result.setResultCode(serviceResult.getResultCode());
            return result;
        }
    }
}
