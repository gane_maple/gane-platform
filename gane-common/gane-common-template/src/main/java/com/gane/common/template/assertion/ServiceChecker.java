package com.gane.common.template.assertion;

import com.gane.common.template.constant.BizErrorCode;
import com.gane.common.template.request.BaseRequest;
import com.gane.common.template.result.BaseResult;

/**
 * @Description TODO
 * @Date 2020/4/27 17:25
 * @Created by 王弘博
 */
public class ServiceChecker {

    public static void checkAndAssert(BaseResult result) {

        AssertUtil.notNull(result, BizErrorCode.SYSTEM_ERROR);

        if (result.isSuccess()) {
            return;
        }

        AssertUtil.isTrue(false, result.getResultCode());

    }
}
