package com.gane.common.template.filter;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public abstract class BeforeFilter extends AbstractFilter {

    @Override
    protected void afterExecute(Object... args) throws Exception {

    }
}
