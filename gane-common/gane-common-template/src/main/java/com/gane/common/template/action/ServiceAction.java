package com.gane.common.template.action;


import com.gane.common.template.result.ServiceResult;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public interface ServiceAction<T> {

    void execute(ServiceResult<T> serviceResult) throws Exception;
}
