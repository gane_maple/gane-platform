package com.gane.common.template.context;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description TODO
 * @Date 2020/4/24 15:44
 * @Created by 王弘博
 */
public class ServiceContextHolder {

    /**
     * 线程变量
     */
    private static ThreadLocal<ServiceContext> serviceContextHolder = new ThreadLocal<>();

    /**
     * 清空线程上下文
     */
    public static void clear() {
        serviceContextHolder.remove();
        ;
    }

    /**
     * 初始化业务上下文
     */
    public static void init() {
        if (serviceContextHolder.get() == null) {
            serviceContextHolder.set(new ServiceContext());
        }
    }

    /**
     * 从线程中获取服务上下文（非空）
     *
     * @return
     */
    public static ServiceContext getServiceContext() {
        return serviceContextHolder.get();
    }

    /**
     * 将服务上下文置到线程中
     *
     * @param serviceContext
     */
    public static void setServiceContext(ServiceContext serviceContext) {

        if (serviceContextHolder.get() == null) {
            serviceContext = new ServiceContext();
        }
        serviceContextHolder.set(serviceContext);
    }

    /**
     * 设置事件码
     *
     * @param eventCode
     */
    public static void setEventCode(String eventCode) {

        ServiceContext serviceContext = serviceContextHolder.get();

        if (serviceContext == null) {
            serviceContext = new ServiceContext();
        }

        serviceContext.setEventCode(eventCode);

        serviceContextHolder.set(serviceContext);
    }

    /**
     * 获取事件码
     *
     * @return
     */
    public static String getEventCode() {

        ServiceContext serviceContext = serviceContextHolder.get();

        if (serviceContext == null) {
            return null;
        }

        return serviceContext.getEventCode();
    }

    /**
     * 设置产品事件码
     *
     * @param pdEventCode
     */
    public static void setPdEventCode(String pdEventCode) {

        ServiceContext serviceContext = serviceContextHolder.get();

        if (serviceContext == null) {
            serviceContext = new ServiceContext();
        }

        serviceContext.setPdEventCode(pdEventCode);

        serviceContextHolder.set(serviceContext);
    }

    /**
     * 获取事件码
     *
     * @return
     */
    public static String getPdEventCode() {

        ServiceContext serviceContext = serviceContextHolder.get();

        if (serviceContext == null) {
            return null;
        }

        return serviceContext.getPdEventCode();
    }

    /**
     * 设置当前时间
     *
     * @param currentTime
     */
    public static void setCurrentTime(Date currentTime) {

        ServiceContext serviceContext = serviceContextHolder.get();

        if (serviceContext == null) {
            serviceContext = new ServiceContext();
        }

        serviceContext.setCurrentTime(currentTime);

        serviceContextHolder.set(serviceContext);
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static Date getCurrentTime() {

        ServiceContext serviceContext = serviceContextHolder.get();

        if (serviceContext == null) {
            return null;
        }

        return serviceContext.getCurrentTime();
    }

    /**
     * 设置扩展信息
     * @param key
     * @param ext
     */
    public static void setExt(String key, Object ext) {

        ServiceContext serviceContext = serviceContextHolder.get();

        if (serviceContext == null) {
            serviceContext = new ServiceContext();
        }

        Map<String, Object> extInfo = serviceContext.getExtInfo();

        if (extInfo == null) {
            serviceContext.setExtInfo(new HashMap<>(3));
        }

        serviceContext.getExtInfo().put(key, ext);

    }

    /**
     * 扩展信息获取具体的内容
     * @param key
     * @return
     */
    public static Object fetchExt(String key) {

        ServiceContext serviceContext = serviceContextHolder.get();

        if (serviceContext == null || serviceContext.getExtInfo() == null) {
            return null;
        }

        return serviceContext.getExtInfo().get(key);

    }
}
