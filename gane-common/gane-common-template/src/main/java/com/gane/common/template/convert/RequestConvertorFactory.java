

package com.gane.common.template.convert;

import com.gane.common.template.assertion.AssertUtil;
import com.gane.common.template.constant.BizErrorCode;
import com.gane.common.template.request.BaseRequest;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description request转换类的工厂类
 * @Date 2020/4/25 9:34
 * @Created by 王弘博
 */
public final class RequestConvertorFactory {

    /**
     * request convert 集合
     */
    private static Map<String, RequestConvertor> convertors = new ConcurrentHashMap<>();

    /**
     * 根据一个request获取一个转换类
     *
     * @param request
     * @return
     */
    public static Object[] convertor(BaseRequest request) {
        if (request == null || !convertors.containsKey(request.getClass().getCanonicalName())) {
            return null;
        }

        return getConvertor(request.getClass()).convert(request);

    }

    /**
     * 根据一个request获取一个转换类
     *
     * @param clazz
     * @return
     */
    public static RequestConvertor getConvertor(Class<?> clazz) {

        AssertUtil.isTrue(convertors.containsKey(clazz.getCanonicalName()),
                BizErrorCode.SYSTEM_ERROR, "convertor of ", clazz.getClass().getCanonicalName(), "not found.");

        return convertors.get(clazz.getCanonicalName());
    }

    /**
     * 注册一个请求的convertor
     *
     * @param clazz
     * @param convertor
     */
    public static void register(Class<?> clazz, RequestConvertor convertor) {

        if (convertors.containsKey(clazz.getCanonicalName())) {
            return;
        }

        convertors.put(clazz.getCanonicalName(), convertor);
    }
}
