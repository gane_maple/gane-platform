package com.gane.common.template.exception;

/**
 * @Description TODO
 * @Date 2020/4/24 18:20
 * @Created by 王弘博
 */
public abstract class AssertionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public abstract void setResultCode(AssertionResultCode resultCode);

    public AssertionException() {

    }

    public AssertionException(String msg) {
        super(msg);
    }

    public AssertionException(Throwable cause) {
        super(cause);
    }

    public AssertionException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
