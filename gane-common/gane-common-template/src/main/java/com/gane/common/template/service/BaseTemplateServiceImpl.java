package com.gane.common.template.service;

import com.gane.common.template.constant.BizScenario;
import com.gane.common.template.context.ServiceContextHolder;
import org.springframework.beans.factory.InitializingBean;

import java.util.Date;

/**
 * @Description TODO
 * @Date 2020/4/24 16:04
 * @Created by 王弘博
 */
public abstract class BaseTemplateServiceImpl implements InitializingBean {

    /**
     * 初始化业务上下文
     *
     */
    protected static void initContext(BizScenario bizScenario) {

        ServiceContextHolder.init();

        ServiceContextHolder.setCurrentTime(new Date());

        ServiceContextHolder.setEventCode(bizScenario.getEventCode());

        ServiceContextHolder.setPdEventCode(bizScenario.getPdEventCode());
    }

    /**
     * 清除业务上下文
     *
     */
    protected static void clearContext() {
        ServiceContextHolder.clear();
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
