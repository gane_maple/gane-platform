package com.gane.common.template.context;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @Description TODO
 * @Date 2020/4/24 15:42
 * @Created by 王弘博
 */
@Data
public class ServiceContext implements Serializable {

    /**
     * 事件码
     */
    private String eventCode;

    /**
     * 产品事件码
     */
    private String pdEventCode;

    /**
     * 当前时间
     */
    private Date currentTime;

    /**
     * 业务对象
     */
    private Object bizObject;

    /**
     * 业务扩展信息
     */
    private Map<String, Object> extInfo;
}
