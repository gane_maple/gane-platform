package com.gane.common.template.exception;

/**
 * @Description TODO
 * @Date 2020/4/24 18:21
 * @Created by 王弘博
 */
public interface AssertionResultCode {

    String getResultCode();

    String getResultMsg();
}
