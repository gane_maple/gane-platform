package com.gane.common.template.result;

import lombok.Data;

/**
 * @Description TODO
 * @Date 2020/5/10 11:29
 * @Created by 王弘博
 */
@Data
public class PageQueryResult extends BaseResult{

    private int pageNo;

    private int pageSize;

    private long pageCount;

    private long totalCount;
}
