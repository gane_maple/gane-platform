package com.gane.common.template.convert;

import com.gane.common.template.request.BaseRequest;

/**
 * @Description 请求报文转换接口
 * @Date 2020/4/25 9:16
 * @Created by 王弘博
 */
public interface RequestConvertor {

    /**
     * 请求报文转换为Manage层业务参数
     * @param request
     * @return
     */
    Object[] convert(BaseRequest request);
}
