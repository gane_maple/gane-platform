package com.gane.common.template.exception;

import com.gane.common.template.constant.BizErrorCode;
import lombok.Data;

/**
 * @Description TODO
 * @Date 2020/4/24 12:19
 * @Created by 王弘博
 */
@Data
public class BizException extends AssertionException {

    private BizErrorCode errorCode;

    @Override
    public void setResultCode(AssertionResultCode resultCode) {
        this.errorCode = (BizErrorCode) resultCode;
    }

    public BizException() {
        super();
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(BizErrorCode errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public BizException(BizErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public BizException(BizErrorCode errorCode, Exception e) {
        super(e);
        this.errorCode = errorCode;
    }

    public BizErrorCode getErrorCode() {
        return errorCode;
    }
}
