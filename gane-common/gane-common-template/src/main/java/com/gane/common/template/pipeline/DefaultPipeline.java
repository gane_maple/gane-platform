package com.gane.common.template.pipeline;

import com.gane.common.template.action.Action;
import com.gane.common.template.filter.Filter;

import java.util.LinkedList;
import java.util.List;

/**
 * 执行链
 *
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public class DefaultPipeline implements Pipeline {

    private List<Filter> filters = new LinkedList<>();

    public DefaultPipeline setFilters(List<Filter> filters) {
        return clearFilters().addFilters(filters);
    }

    @Override
    public DefaultPipeline addFilters(List<Filter> filters) {
        if (filters != null) {
            filters.forEach(this::addFilter);
        }
        return this;
    }

    @Override
    public DefaultPipeline addFilter(Filter filter) {
        filters.add(0, filter);
        return this;
    }

    @Override
    public DefaultPipeline clearFilters() {
        filters.clear();
        return this;
    }

    @Override
    public <V> V execute(Action<V> action, Object... args) throws Exception {
        return buildChain(action).execute(args);
    }

    private <V> Action<V> buildChain(Action<V> action) {
        Action<V> last = action;
        for (Filter filter : filters) {
            Action<V> next = last;
            last = args -> filter.execute(next, args);
        }
        return last;
    }
}
