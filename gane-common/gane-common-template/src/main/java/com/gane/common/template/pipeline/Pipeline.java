package com.gane.common.template.pipeline;

import com.gane.common.template.action.Action;
import com.gane.common.template.filter.Filter;

import java.util.List;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public interface Pipeline {

    Pipeline addFilters(List<Filter> filters);

    Pipeline addFilter(Filter filter);

    Pipeline clearFilters();

    <V> V execute(Action<V> action, Object... args) throws Exception;
}
