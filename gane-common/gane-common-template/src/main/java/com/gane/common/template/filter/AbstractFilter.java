package com.gane.common.template.filter;

import com.gane.common.template.action.Action;

/**
 * @author 王弘博
 * @create 2019-05-08 10:06
 */
public abstract class AbstractFilter implements Filter {

    protected abstract void beforeExecute(Object... args) throws Exception;

    protected abstract void afterExecute(Object... args) throws Exception;

    @Override
    public <V> V execute(Action<V> action, Object... args) throws Exception {
        beforeExecute(args);
        V result = action.execute(args);
        afterExecute(args);
        return result;
    }
}
