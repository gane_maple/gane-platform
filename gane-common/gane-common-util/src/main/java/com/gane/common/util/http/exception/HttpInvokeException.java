package com.gane.common.util.http.exception;

/**
 * @Description TODO
 * @Date 2019/8/13 16:12
 * @Created by 王弘博
 */
public class HttpInvokeException extends RuntimeException {

    private static final long serialVersionUID = 8878632697014781457L;

    public HttpInvokeException(String message) {
        super(message);
    }

    public HttpInvokeException(String message, Throwable cause) {
        super(message, cause);
    }
}
