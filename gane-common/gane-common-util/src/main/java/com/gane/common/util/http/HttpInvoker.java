package com.gane.common.util.http;

import com.gane.common.util.http.exception.HttpInvokeException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;


/**
 * @Description HttpClient的友好工具类，推荐使用长连接
 * @Date 2019/8/13 16:14
 * @Created by 王弘博
 */
public class HttpInvoker {

    public static final ResponseHandler<String> STRING_ENTITY_HANDLER = new StringEntityHandler(Consts.UTF_8);

    // 链接配置
    private int maxConnections = 16;
    private int connectionRequestTimeout = 3000;
    private int connectTimeout = 1000;
    private int socketTimeout = 3000;

    // SSL证书配置
    private String keyStore;
    private String keyStorePassword;
    private String keyPassword;
    private boolean trusted;
    private String trustKeyStore;
    private String trustKeyStorePassword;
    private String[] protocols = new String[]{"TLSv1", "TLSv1.1", "TLSv1.2"};
    private String[] cipherSuites = null;

    private CloseableHttpClient httpClient;

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public void setConnectionRequestTimeout(int connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public void setKeyStore(String keyStore) {
        this.keyStore = keyStore;
    }

    public void setKeyStorePassword(String keyStorePassword) {
        this.keyStorePassword = keyStorePassword;
    }

    public void setKeyPassword(String keyPassword) {
        this.keyPassword = keyPassword;
    }

    public void setTrusted(boolean trusted) {
        this.trusted = trusted;
    }

    public void setTrustKeyStore(String trustKeyStore) {
        this.trustKeyStore = trustKeyStore;
    }

    public void setTrustKeyStorePassword(String trustKeyStorePassword) {
        this.trustKeyStorePassword = trustKeyStorePassword;
    }

    public void setProtocols(String[] protocols) {
        this.protocols = protocols;
    }

    public void setCipherSuites(String[] cipherSuites) {
        this.cipherSuites = cipherSuites;
    }

    /**
     * init，使用之前调用
     */
    public void init() {
        HttpClientBuilder httpClientBuilder = HttpClients.custom()
                .setMaxConnTotal(maxConnections)
                .setMaxConnPerRoute(maxConnections)
                .setRetryHandler(new DefaultHttpRequestRetryHandler());
        configHttpClientBuilder(httpClientBuilder);
        httpClient = httpClientBuilder.build();
    }

    /**
     * close，如果使用短连接，则每次用完就要调用close
     */
    public void close() {
        if (httpClient != null) {
            try {
                httpClient.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    protected void configHttpClientBuilder(HttpClientBuilder httpClientBuilder) {
        httpClientBuilder.disableAutomaticRetries()
                .disableRedirectHandling()
                .disableCookieManagement()
                .disableConnectionState(); // 在SSL长链接环境下，禁用
        configSSLContext(httpClientBuilder);
    }

    protected void configSSLContext(HttpClientBuilder httpClientBuilder) {
        SSLContext sslContext = null;
        try {
            SSLContextBuilder sslContextBuilder = SSLContexts.custom();
            if (StringUtils.isNotBlank(keyStore)) {
                char[] storepwd = keyStorePassword != null ? keyStorePassword.toCharArray() : new char[0];
                char[] keypwd = keyPassword != null ? keyPassword.toCharArray() : new char[0];
                sslContextBuilder.loadKeyMaterial(new File(keyStore), storepwd, keypwd);
            }
            if (trusted) {
                sslContextBuilder.loadTrustMaterial(new TrustStrategy() {
                    public boolean isTrusted(X509Certificate[] chain, String authType) {
                        return true;
                    }
                });
            } else if (StringUtils.isNotBlank(trustKeyStore)) {
                char[] storepwd = trustKeyStorePassword != null ? trustKeyStorePassword.toCharArray() : new char[0];
                sslContextBuilder.loadTrustMaterial(new File(trustKeyStore), storepwd);
            }
            sslContext = sslContextBuilder.build();
        } catch (GeneralSecurityException | IOException e) {
            throw new IllegalArgumentException(e);
        }
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, protocols, cipherSuites, NoopHostnameVerifier.INSTANCE);
        httpClientBuilder.setSSLSocketFactory(sslsf);
    }

    protected void configRequestBuilder(RequestConfig.Builder builder) {
        builder.setRedirectsEnabled(false);
    }

    /**
     * invoke
     *
     * @param request
     * @param handler callback
     * @return
     */
    public <T> T invoke(HttpRequestBase request, ResponseHandler<T> handler) {
        return invoke(request, null, handler);
    }

    /**
     * invoke with context
     *
     * @param request
     * @param context
     * @param handler
     * @return
     */
    public <T> T invoke(HttpRequestBase request, HttpContext context, ResponseHandler<T> handler) {
        Validate.notNull(request);
        Validate.notNull(handler);
        if (request.getConfig() == null) {
            RequestConfig.Builder requestConfigBuilder = RequestConfig.custom()
                    .setConnectionRequestTimeout(connectionRequestTimeout)
                    .setConnectTimeout(connectTimeout)
                    .setSocketTimeout(socketTimeout);
            configRequestBuilder(requestConfigBuilder);
            request.setConfig(requestConfigBuilder.build());
        }
        try {
            return httpClient.execute(request, handler, context);
        } catch (IOException e) {
            throw new HttpInvokeException("Http invoke occur error.", e);
        }
    }

    /**
     * String handler
     *
     * @author 潘海春
     * @date 2016年8月24日
     */
    public static class StringEntityHandler implements ResponseHandler<String> {

        private Charset charset;

        public StringEntityHandler() {
            this(Consts.UTF_8);
        }

        public StringEntityHandler(String charset) {
            this(Charset.forName(charset));
        }

        public StringEntityHandler(Charset charset) {
            this.charset = charset;
        }

        @Override
        public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
            int status = response.getStatusLine().getStatusCode();
            if (status >= HttpStatus.SC_BAD_REQUEST) {
                throw new HttpInvokeException("Http invoke failure, http status is " + status);
            }
            return EntityUtils.toString(response.getEntity(), charset);
        }

    }

    ;
}
