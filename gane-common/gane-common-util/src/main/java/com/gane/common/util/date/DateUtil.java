package com.gane.common.util.date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * @Description TODO
 * @Date 2019/8/17 16:36
 * @Created by 王弘博
 */
public class DateUtil {

    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static final String YYYYMMDDHHMMSS_WHTH_BAR = "yyyy-MM-dd HH:mm:ss";

    public static Date parse(String date, String... format) {
        try {
            return DateUtils.parseDate(date, format);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static String format(Date date, String format) {
        return DateFormatUtils.format(date, format);
    }
}
