package com.gane.common.util.http;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.concurrent.Future;

/**
 * @Description HttpClient 异步工具类，推荐使用长连接
 * @Date 2019/8/13 16:12
 * @Created by 王弘博
 */
public class HttpAsyncInvoker {

    // 链接配置
    private int maxConnections = 16;
    private int connectionRequestTimeout = 3000;
    private int connectTimeout = 1000;
    private int socketTimeout = 3000;
    private IOReactorConfig.Builder ioReactorBuilder = IOReactorConfig.custom();

    // SSL证书配置
    private String keyStore;
    private String keyStorePassword;
    private String keyPassword;
    private boolean trusted;
    private String trustKeyStore;
    private String trustKeyStorePassword;

    private CloseableHttpAsyncClient httpClient;

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public void setConnectionRequestTimeout(int connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public void setIoThreadCount(int ioThreadCount) {
        ioReactorBuilder.setIoThreadCount(ioThreadCount);
    }

    public void setKeyStore(String keyStore) {
        this.keyStore = keyStore;
    }

    public void setKeyStorePassword(String keyStorePassword) {
        this.keyStorePassword = keyStorePassword;
    }

    public void setKeyPassword(String keyPassword) {
        this.keyPassword = keyPassword;
    }

    public void setTrusted(boolean trusted) {
        this.trusted = trusted;
    }

    public void setTrustKeyStore(String trustKeyStore) {
        this.trustKeyStore = trustKeyStore;
    }

    public void setTrustKeyStorePassword(String trustKeyStorePassword) {
        this.trustKeyStorePassword = trustKeyStorePassword;
    }

    /**
     * init，使用之前调用
     */
    public void init() {
        HttpAsyncClientBuilder httpClientBuilder = HttpAsyncClients.custom()
                .setMaxConnTotal(maxConnections)
                .setMaxConnPerRoute(maxConnections)
                .setDefaultIOReactorConfig(ioReactorBuilder.build());
        configHttpClientBuilder(httpClientBuilder);
        httpClient = httpClientBuilder.build();
        httpClient.start();
    }

    /**
     * close，如果使用短连接，则每次用完就要调用close
     */
    public void close() {
        if (httpClient != null) {
            try {
                httpClient.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    protected void configHttpClientBuilder(HttpAsyncClientBuilder httpClientBuilder) {
        httpClientBuilder.disableCookieManagement()
                .disableConnectionState(); // 在SSL长链接环境下，禁用
        configSSLContext(httpClientBuilder);
    }

    protected void configSSLContext(HttpAsyncClientBuilder httpClientBuilder) {
        SSLContext sslContext = null;
        try {
            SSLContextBuilder sslContextBuilder = SSLContexts.custom();
            if (StringUtils.isNotBlank(keyStore)) {
                char[] storepwd = keyStorePassword != null ? keyStorePassword.toCharArray() : new char[0];
                char[] keypwd = keyPassword != null ? keyPassword.toCharArray() : new char[0];
                sslContextBuilder.loadKeyMaterial(new File(keyStore), storepwd, keypwd);
            }
            if (trusted) {
                sslContextBuilder.loadTrustMaterial(new TrustStrategy() {
                    @Override
                    public boolean isTrusted(X509Certificate[] chain, String authType) {
                        return true;
                    }
                });
            } else if (StringUtils.isNotBlank(trustKeyStore)) {
                char[] storepwd = trustKeyStorePassword != null ? trustKeyStorePassword.toCharArray() : new char[0];
                sslContextBuilder.loadTrustMaterial(new File(trustKeyStore), storepwd);
            }
            sslContext = sslContextBuilder.build();
        } catch (GeneralSecurityException | IOException e) {
            throw new IllegalArgumentException(e);
        }
        httpClientBuilder.setSSLContext(sslContext).setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).setRedirectStrategy(new DefaultRedirectStrategy() {
            @Override
            public boolean isRedirected(HttpRequest request, HttpResponse response, HttpContext context) throws ProtocolException {
                // 禁用重定向
                return false;
            }
        });
    }

    protected void configRequestBuilder(RequestConfig.Builder builder) {
        builder.setRedirectsEnabled(false);
    }

    /**
     * invoke
     *
     * @param request
     * @return
     */
    public Future<HttpResponse> invoke(HttpRequestBase request) {
        return invoke(request, null, null);
    }

    /**
     * invoke
     *
     * @param request
     * @param callback
     * @return
     */
    public Future<HttpResponse> invoke(HttpRequestBase request, FutureCallback<HttpResponse> callback) {
        return invoke(request, null, callback);
    }

    /**
     * invoke with context
     *
     * @param request
     * @param context
     * @param callback
     * @return
     */
    public Future<HttpResponse> invoke(HttpRequestBase request, HttpContext context, FutureCallback<HttpResponse> callback) {
        Validate.notNull(request);
        if (request.getConfig() == null) {
            RequestConfig.Builder requestConfigBuilder = RequestConfig.custom()
                    .setConnectionRequestTimeout(connectionRequestTimeout)
                    .setConnectTimeout(connectTimeout)
                    .setSocketTimeout(socketTimeout);
            configRequestBuilder(requestConfigBuilder);
            request.setConfig(requestConfigBuilder.build());
        }
        return httpClient.execute(request, context, callback);
    }
}
