# gane-platform

#### Description
简一管理平台（继续更新中...）

1、技术：
nacos + zuul + zipkin + xxl-job + SpringbootAdmin + security + oauth2 + lcn + redis + mybatisplus + mysql

2、目的:
优雅的写出一个管理平台脚手架，参考http://www.ruoyi.vip/  的页面与表
中小公司拿来即用，在公共功能上不浪费时间，专注自己的业务开发


#### Software Architecture
Software architecture description

#### Installation

想要跑起来该项目，需要自己安装nacos、mysql、redis、安装jdk、maven等以及环境变量。
导入工程里的sql

-------------------------Windows下--------------------------

1、把项目中的mysql、redis等地址换成自己的

2、需要在hosts文件中增加DNS映射

127.0.0.1 gane-nacos

3、首先nacos要可运行

访问http://gane-nacos:8848/nacos，输入账密 nacos/nacos

出现nacos面板则表示安装、启动成功。

然后依次打包并启动zuul、lcn、SpringbootAdmin、auth、admin等服务。

服务-端口

gane-nacos：8848

gane-zuul：11004

gane-auth：11006

gane-admin：11007

gane-lcn：11008

gane-springboot-admin：11009


测试的时候，需要先登录：

(1)、http://gane-nacos:11004/gane-auth/oauth/token?grant_type=password&username=maple&password=123456

拿到access_token：5d47bcf8-020a-4089-8420-e0ba5b66a8fe

(2)、查询部门树

http://gane-nacos:11004/gane-admin/department/tree?access_token=5d47bcf8-020a-4089-8420-e0ba5b66a8fe

(3)、访问SpringbootAdmin监控：

http://gane-nacos:11004/gane-springboot-admin

(4)、访问druid监控

http://gane-nacos:11004/gane-admin/druid/

(5)、访问Txmanager:

http://gane-nacos:11004/gane-lcn


-------------------------Linux下--------------------------

Linux下：需要准备三台服务器，并在 /etc/hosts 文件里分别配置
192.168.40.133 gane-nacos

其余操作同上

#### Instructions

1.  目前项目由我个人开发中，业务代码我不打算每个功能都写，但我都会给出demo
2.  zipkin + xxl-job 功能暂未开发
3.  需要把eureka+config换成nacos（完成）
4.  项目暂时没有写前端页面

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
