# gane-platform

#### Description
简一管理平台（继续更新中...）

1、技术：
eureka + zuul + zipkin + SpringbootAdmin + security + oauth2 + lcn + redis + mybatisplus + mysql

2、目的:
优雅的写出一个管理平台脚手架，参考http://www.ruoyi.vip/的页面与表
中小公司拿来即用，在公共功能上不浪费时间，专注自己的业务开发


#### Software Architecture
Software architecture description

#### Installation

想要跑起来该项目，需要自己安装mysql、redis、安装jdk、maven等以及环境变量。
导入工程里的sql

-------------------------Windows下--------------------------

1、把项目中的mysql、redis等地址换成自己的

2、需要在hosts文件中增加DNS映射
127.0.0.1 gane-eureka1
127.0.0.1 gane-eureka2
127.0.0.1 gane-eureka3

3、首先打包eureka的jar，然后cmd打开三个黑窗口，执行
java -jar gane-eureka.jar --spring.profiles.active=server1
java -jar gane-eureka.jar --spring.profiles.active=server2
java -jar gane-eureka.jar --spring.profiles.active=server3
模拟高可用，都启动成功之后，浏览器访问 http://gane-eureka1:11001/
出现eureka面板则表示启动成功。
然后依次打包并启动zuul、lcn、SpringbootAdmin、auth、admin等服务。

服务-端口
gane-eureka：11001
gane-eureka：11002
gane-eureka：11003

gane-zuul：11004

gane-auth：11006

gane-admin：11007

gane-lcn：11008

gane-springboot-admin：11009


测试的时候，需要先登录：

(1)、http://gane-eureka1:11004/gane-auth/oauth/token?grant_type=password&username=maple&password=123456

拿到access_token：5d47bcf8-020a-4089-8420-e0ba5b66a8fe

(2)、查询部门树

http://gane-eureka1:11004/gane-admin/department/tree?access_token=5d47bcf8-020a-4089-8420-e0ba5b66a8fe

(3)、访问SpringbootAdmin监控：

http://gane-eureka1:11004/gane-springboot-admin

(4)、访问druid监控

http://gane-eureka1:11004/gane-admin/druid/

(5)、访问Txmanager:

http://gane-eureka1:11004/gane-lcn


-------------------------Linux下--------------------------

Linux下：需要准备三台服务器，并在 /etc/hosts 文件里分别配置
192.168.40.100 gane-eureka1
192.168.40.101 gane-eureka2
192.168.40.102 gane-eureka3

其余操作同上

#### Instructions

1.  目前项目由我个人开发中，业务代码我不打算每个功能都写，但我都会给出demo
2.  zipkin功能暂未开发
3.  需要把eureka+config换成nacos
4.  项目暂时没有写前端页面

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
