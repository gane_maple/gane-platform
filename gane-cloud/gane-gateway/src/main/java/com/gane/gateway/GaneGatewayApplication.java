package com.gane.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableDiscoveryClient
@EnableZuulProxy
@SpringBootApplication
@EnableOAuth2Sso
public class GaneGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GaneGatewayApplication.class, args);
		System.out.println("----------gane-zuul启动成功！");
	}
}
