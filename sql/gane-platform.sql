/*
Navicat MySQL Data Transfer

Source Server         : 我的服务器
Source Server Version : 50729
Source Host           : 106.15.184.65:3306
Source Database       : gane-platform

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2020-05-24 18:09:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` int(11) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(64) DEFAULT '' COMMENT '祖级列表',
  `department_name` varchar(32) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NOT NULL DEFAULT '0' COMMENT '显示顺序',
  `leader_id` int(11) NOT NULL COMMENT '负责人编号',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `create_by` int(11) NOT NULL COMMENT '创建者',
  `update_by` int(11) NOT NULL COMMENT '更新者',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 创建时间 ',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT ' 更新时间 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='部门表';

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('100', '0', '0', '简一电子商务商行', '0', '1', '0', '1', '1', '2020-04-29 16:31:30', '2020-04-29 16:31:30');
INSERT INTO `department` VALUES ('101', '100', '0,100', '杭州总公司', '1', '1', '0', '1', '1', '2020-04-29 16:32:13', '2020-04-29 16:32:13');
INSERT INTO `department` VALUES ('102', '100', '0,100', '上海分公司', '2', '1', '0', '1', '1', '2020-04-29 16:33:09', '2020-04-29 16:33:23');
INSERT INTO `department` VALUES ('1000', '101', '0,100,101', '技术部门', '1', '1', '0', '1', '1', '2020-04-29 16:34:53', '2020-04-29 16:35:13');
INSERT INTO `department` VALUES ('1002', '101', '0,100,101', '销售部门', '2', '1', '0', '1', '1', '2020-04-29 16:37:16', '2020-04-29 16:37:16');
INSERT INTO `department` VALUES ('1003', '102', '0,100,102', '技术部门', '1', '1', '0', '1', '1', '2020-04-29 16:36:22', '2020-04-29 16:37:21');
INSERT INTO `department` VALUES ('1004', '102', '0,100,102', '财务部门', '2', '1', '0', '1', '1', '2020-04-29 16:38:04', '2020-04-29 16:38:04');
INSERT INTO `department` VALUES ('1005', '101', '0,100,101', '运维部门', '3', '1', '0', '1', '1', '2020-05-22 13:25:56', '2020-05-23 21:02:35');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(32) NOT NULL COMMENT '菜单名称',
  `menu_key` varchar(32) NOT NULL COMMENT '菜单标识',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) NOT NULL DEFAULT '0' COMMENT '显示顺序',
  `menu_type` char(1) NOT NULL COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `redirect` varchar(32) DEFAULT '' COMMENT '重定向',
  `perms` varchar(64) NOT NULL DEFAULT '' COMMENT '权限标识',
  `icon` varchar(64) NOT NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` int(11) NOT NULL COMMENT '创建者',
  `update_by` int(11) NOT NULL COMMENT '更新者',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 创建时间 ',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT ' 更新时间 ',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='菜单权限表';

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '系统管理', 'system', '0', '0', 'M', '', '', 'setting', '1', '1', '2020-04-29 16:15:21', '2020-04-29 16:16:57', '');
INSERT INTO `menu` VALUES ('2', '系统监控', 'monitor', '0', '0', 'M', '', '', 'video-camera', '1', '1', '2020-04-29 16:15:50', '2020-04-29 16:17:03', '');
INSERT INTO `menu` VALUES ('3', '系统工具', 'tool', '0', '0', 'M', '', '', 'bars', '1', '1', '2020-04-29 16:16:08', '2020-04-29 16:17:08', '');
INSERT INTO `menu` VALUES ('100', '用户管理', 'user', '1', '1', 'C', 'user/load', 'user:view', '#', '1', '1', '2020-04-29 16:18:01', '2020-05-21 22:45:56', '');
INSERT INTO `menu` VALUES ('101', '角色管理', 'role', '1', '2', 'C', 'role/load', 'role:view', '#', '1', '1', '2020-04-29 16:20:02', '2020-05-21 22:47:23', '');
INSERT INTO `menu` VALUES ('102', '菜单管理', 'menu', '0', '3', 'C', 'menu/load', 'menu:view', '#', '1', '1', '2020-05-21 22:44:58', '2020-05-21 22:49:02', '');
INSERT INTO `menu` VALUES ('103', '部门管理', 'department', '0', '4', 'C', 'department/load', 'department:view', '#', '1', '1', '2020-05-21 22:48:15', '2020-05-21 22:48:44', '');
INSERT INTO `menu` VALUES ('1000', '用户查询', 'user query', '100', '1', 'F', 'user/load', 'user:view', '#', '1', '1', '2020-04-29 16:23:02', '2020-05-22 13:02:00', '');
INSERT INTO `menu` VALUES ('1001', '用户新增', 'user add', '100', '2', 'F', 'user/add', 'user:add', '#', '1', '1', '2020-05-21 22:49:51', '2020-05-22 13:02:32', '');
INSERT INTO `menu` VALUES ('1003', '部门查询', 'department query', '103', '1', 'F', 'department/load', 'department:view', '#', '1', '1', '2020-05-21 22:50:43', '2020-05-21 22:50:59', '');
INSERT INTO `menu` VALUES ('1004', '部门树', 'department tree', '103', '2', 'F', 'department/tree', 'department:tree', '#', '1', '1', '2020-05-21 22:51:54', '2020-05-21 22:51:54', '');
INSERT INTO `menu` VALUES ('1005', '部门新增', 'department add', '103', '3', 'F', 'department/add', 'department:add', '#', '1', '1', '2020-05-21 22:53:14', '2020-05-21 22:53:14', '');
INSERT INTO `menu` VALUES ('1006', '部门修改', 'department update', '103', '4', 'F', 'department/update', 'department:update', '#', '1', '1', '2020-05-21 22:54:02', '2020-05-21 22:55:04', '');
INSERT INTO `menu` VALUES ('1007', '部门删除', 'department delete', '103', '5', 'F', 'department/delete', 'department:delete', '#', '1', '1', '2020-05-21 22:54:45', '2020-05-21 22:54:53', '');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(32) NOT NULL COMMENT '角色名称',
  `role_key` varchar(32) NOT NULL COMMENT '角色标识',
  `order_num` int(4) NOT NULL DEFAULT '0' COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `create_by` int(11) NOT NULL COMMENT '创建者',
  `update_by` int(11) NOT NULL COMMENT '更新者',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 创建时间 ',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT ' 更新时间 ',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT=' 角色表 ';

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '管理员', 'admin', '0', '1', '0', '1', '1', '2020-04-29 17:17:57', '2020-04-29 17:17:57', '');
INSERT INTO `role` VALUES ('2', '普通用户', 'user', '1', '1', '0', '1', '1', '2020-04-29 17:18:09', '2020-04-29 17:18:09', '');

-- ----------------------------
-- Table structure for role_department
-- ----------------------------
DROP TABLE IF EXISTS `role_department`;
CREATE TABLE `role_department` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `department_id` int(11) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`department_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of role_department
-- ----------------------------
INSERT INTO `role_department` VALUES ('2', '101');
INSERT INTO `role_department` VALUES ('2', '1000');

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES ('2', '1');
INSERT INTO `role_menu` VALUES ('2', '2');
INSERT INTO `role_menu` VALUES ('2', '3');
INSERT INTO `role_menu` VALUES ('2', '100');
INSERT INTO `role_menu` VALUES ('2', '101');
INSERT INTO `role_menu` VALUES ('2', '102');
INSERT INTO `role_menu` VALUES ('2', '103');
INSERT INTO `role_menu` VALUES ('2', '1000');
INSERT INTO `role_menu` VALUES ('2', '1001');
INSERT INTO `role_menu` VALUES ('2', '1003');
INSERT INTO `role_menu` VALUES ('2', '1004');
INSERT INTO `role_menu` VALUES ('2', '1005');
INSERT INTO `role_menu` VALUES ('2', '1006');
INSERT INTO `role_menu` VALUES ('2', '1007');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `department_id` int(11) NOT NULL COMMENT '部门ID',
  `login_name` varchar(32) NOT NULL COMMENT '登录账号',
  `nick_name` varchar(32) NOT NULL COMMENT '昵称，花名',
  `user_type` varchar(2) NOT NULL COMMENT '用户类型（00系统管理员，01为普通用户）',
  `email` varchar(64) DEFAULT '' COMMENT '邮箱',
  `mobile` varchar(11) NOT NULL COMMENT '手机号码',
  `sex` char(1) NOT NULL DEFAULT '0' COMMENT '性别（0男 1女 2未知）',
  `password` varchar(512) NOT NULL COMMENT '登录密码',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `avatar` varchar(128) DEFAULT '' COMMENT '头像路径',
  `create_by` int(11) NOT NULL COMMENT '创建者',
  `update_by` int(11) NOT NULL COMMENT '更新者',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 创建时间 ',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT ' 更新时间 ',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT=' 用户表 ';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', 'maple', 'maple', '00', '1', '1', '0', '$2a$10$1N9YJbFtGzLZyx207cvsIe6c/jMxLtd0wjsvueN7j/UNmei14iD9.', '0', '', '1', '1', '2020-04-29 10:11:37', '2020-05-09 22:43:19', '');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL COMMENT '管理员ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', '1');
INSERT INTO `user_role` VALUES ('1', '2');
