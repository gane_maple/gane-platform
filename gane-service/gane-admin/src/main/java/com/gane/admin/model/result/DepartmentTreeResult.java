package com.gane.admin.model.result;

import com.gane.admin.model.vo.DepartmentTreeVO;
import com.gane.common.template.result.BaseResult;
import lombok.Data;

import java.util.List;

/**
 * @Description TODO
 * @Date 2020/5/10 11:31
 * @Created by 王弘博
 */
@Data
public class DepartmentTreeResult extends BaseResult {

    private List<DepartmentTreeVO> resultList;
}
