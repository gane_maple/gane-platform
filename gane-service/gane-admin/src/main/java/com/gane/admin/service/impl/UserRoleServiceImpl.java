package com.gane.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gane.admin.dao.UserRoleDAO;
import com.gane.admin.model.entity.UserRoleDO;
import com.gane.admin.service.UserRoleService;
import org.springframework.stereotype.Service;

/**
 * @Description AdminRoleServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleDAO, UserRoleDO> implements UserRoleService {
}
