package com.gane.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gane.admin.dao.UserDAO;
import com.gane.admin.model.entity.UserDO;
import com.gane.admin.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @Description UserServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDAO, UserDO> implements UserService {
}
