package com.gane.admin.model.convert.user;

import com.gane.admin.model.convert.BaseRequestConvertor;
import com.gane.admin.model.request.UserCreateRequest;
import com.gane.common.template.assertion.AssertUtil;
import com.gane.common.template.constant.BizErrorCode;
import com.gane.common.template.request.BaseRequest;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @Description 创建user的转换类
 * @Date 2020/4/25 9:49
 * @Created by 王弘博
 */
@Component
public class UserCreateRequestConvertor extends BaseRequestConvertor {

    @Override
    public Object[] convert(BaseRequest request) {

        AssertUtil.isTrue((request instanceof UserCreateRequest),
                BizErrorCode.SYSTEM_ERROR, "illegal request type");

        return Arrays.asList(request).toArray();
    }

    @Override
    protected Class<?> getRequestClass() {
        return UserCreateRequest.class;
    }
}
