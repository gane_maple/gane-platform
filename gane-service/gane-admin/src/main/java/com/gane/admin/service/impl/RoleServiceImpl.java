package com.gane.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gane.admin.dao.RoleDAO;
import com.gane.admin.model.entity.RoleDO;
import com.gane.admin.service.RoleService;
import org.springframework.stereotype.Service;

/**
 * @Description RoleServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDAO, RoleDO> implements RoleService {
}
