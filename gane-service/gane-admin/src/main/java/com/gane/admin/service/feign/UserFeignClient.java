package com.gane.admin.service.feign;

import com.gane.admin.dto.PermssionDTO;
import com.gane.admin.dto.RoleDTO;
import com.gane.admin.dto.UserDTO;
import com.gane.admin.feign.IUserFeignClient;
import com.gane.admin.manager.RoleAndPermManager;
import com.gane.admin.manager.UserManager;
import com.gane.common.template.exception.BizException;
import com.gane.common.template.result.ServiceResult;
import com.gane.common.template.service.ServiceTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description TODO
 * @Date 2020/4/27 16:40
 * @Created by 王弘博
 */
@RestController("/user")
public class UserFeignClient implements IUserFeignClient {

    @Resource
    private ServiceTemplate serviceTemplate;

    @Resource
    private UserManager userManager;

    @Resource
    private RoleAndPermManager roleAndPermManager;

    @GetMapping("/queryByLoginName")
    @Override
    public ServiceResult<UserDTO> queryByLoginName(String loginName) {

        return serviceTemplate.execute(result -> {

            ServiceResult<UserDTO> serviceResult = userManager.queryByLoginName(loginName);

            result.setResultObj(serviceResult.getResultObj());

        }, (result, ex) -> {
            return handleException(result, ex);
        });

    }

    @GetMapping("/queryRolesByUserId")
    @Override
    public ServiceResult queryRolesByUserId(Integer userId) {

        return serviceTemplate.execute(result -> {

            ServiceResult<List<RoleDTO>> serviceResult = roleAndPermManager.queryRolesByUserId(userId);

            result.setResultObj(serviceResult.getResultObj());

        }, (result, ex) -> {
            return handleException(result, ex);
        });

    }

    @GetMapping("/queryPermsByRoleIds")
    @Override
    public ServiceResult queryPermsByRoleIds(List<Integer> roleIds) {

        return serviceTemplate.execute(result -> {

            ServiceResult<List<PermssionDTO>> serviceResult = roleAndPermManager.queryPermsByRoleIds(roleIds);

            result.setResultObj(serviceResult.getResultObj());

        }, (result, ex) -> {
            return handleException(result, ex);
        });
    }

    private <T> boolean handleException(ServiceResult<T> r, Exception e) {

        if (e instanceof BizException) {

            r.setResultCode(((BizException) e).getErrorCode());

            r.setSuccess(false);

            return true;
        }

        //非业务异常，往上抛
        return false;
    }
}
