package com.gane.admin.manager;

import com.gane.admin.dto.PermssionDTO;
import com.gane.admin.dto.RoleDTO;
import com.gane.common.template.result.ServiceResult;

import java.util.List;

/**
 * @Description TODO
 * @Date 2020/4/29 19:55
 * @Created by 王弘博
 */
public interface RoleAndPermManager {

    ServiceResult<List<RoleDTO>> queryRolesByUserId(Integer userId);

    ServiceResult<List<PermssionDTO>> queryPermsByRoleIds(List<Integer> roleIds);
}
