package com.gane.admin.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gane.admin.common.constants.AdminConstants;
import com.gane.admin.manager.DepartmentManager;
import com.gane.admin.manager.UserManager;
import com.gane.admin.model.entity.DepartmentDO;
import com.gane.admin.model.request.department.DepartmentCreateRequest;
import com.gane.admin.model.request.department.DepartmentDeleteRequest;
import com.gane.admin.model.request.department.DepartmentLoadRequest;
import com.gane.admin.model.vo.DepartmentLoadVO;
import com.gane.admin.model.vo.DepartmentTreeVO;
import com.gane.admin.service.DepartmentService;
import com.gane.admin.service.UserService;
import com.gane.common.template.assertion.AssertUtil;
import com.gane.common.template.constant.BizErrorCode;
import com.gane.common.template.context.ServiceContextHolder;
import com.gane.common.template.result.ServiceResult;
import com.gane.common.template.service.ServiceCallback;
import com.gane.common.template.service.ServiceTemplateImpl;
import com.gane.common.util.date.DateUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Date 2020/5/20 13:38
 * @Created by 王弘博
 */
@Service
public class DepartmentManagerImpl implements DepartmentManager {

    @Resource
    private UserService userService;

    @Resource
    private UserManager userManager;

    @Resource
    private DepartmentService departmentService;

    @Override
    public ServiceResult<List<DepartmentLoadVO>> queryDepartment(DepartmentLoadRequest request) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback<List<DepartmentLoadVO>>() {

            @Override
            public ServiceResult<List<DepartmentLoadVO>> executeService() {

                QueryWrapper<DepartmentDO> queryWrapper = new QueryWrapper<>();

                if (StringUtils.isNotBlank(request.getDepartmentName())) {
                    queryWrapper.eq("department_name", request.getDepartmentName());
                }

                if (StringUtils.isNotBlank(request.getStatus())) {
                    queryWrapper.like("status", request.getStatus());
                }

                return ServiceResult.valueOfSuccess(convert(departmentService.list(queryWrapper)));
            }
        });
    }

    @Override
    public ServiceResult<List<DepartmentTreeVO>> queryDepartmentTree() {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback<List<DepartmentTreeVO>>() {

            @Override
            public ServiceResult<List<DepartmentTreeVO>> executeService() {

                List<DepartmentTreeVO> resultList = new ArrayList<>(1);

                List<DepartmentDO> departmentDOS = departmentService.list(new QueryWrapper<DepartmentDO>().eq("parent_id", 0));

                if (CollectionUtils.isEmpty(departmentDOS)) {

                    return ServiceResult.valueOfSuccess(resultList);
                }

                for (DepartmentDO departmentDO : departmentDOS) {

                    DepartmentTreeVO vo = new DepartmentTreeVO();

                    BeanUtils.copyProperties(departmentDO, vo);

                    vo.setChirdDepartmentList(getChildDepartment(departmentDO.getId()));

                    resultList.add(vo);
                }

                return ServiceResult.valueOfSuccess(resultList);
            }
        });
    }

    @Override
    public ServiceResult createDepartment(DepartmentCreateRequest request) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback<Boolean>() {

            @Override
            public void beforeService() {

                createDepartmentPreCheck(request);
            }

            @Override
            public ServiceResult<Boolean> executeService() {

                return ServiceResult.valueOfSuccess(departmentService.save(covert(request)));
            }
        });
    }

    private DepartmentDO covert(DepartmentCreateRequest request) {

        DepartmentDO departmentDO = new DepartmentDO();

        DepartmentDO parentDepartment = (DepartmentDO) ServiceContextHolder.fetchExt(AdminConstants.PARENT_DEPARTMENT);

        departmentDO.setAncestors(parentDepartment.getAncestors() + "," + parentDepartment.getId());
        departmentDO.setDepartmentName(request.getDepartmentName());
        departmentDO.setLeaderId(request.getLeaderId());
        departmentDO.setOrderNum(request.getOrderNum());
        departmentDO.setParentId(request.getParentDepartmentId());
        departmentDO.setStatus(request.getStatus());
        departmentDO.setCreateBy(userManager.currentUser().getUserId());
        departmentDO.setUpdateBy(userManager.currentUser().getUserId());
        departmentDO.setCreateTime(ServiceContextHolder.getCurrentTime());
        departmentDO.setUpdateTime(new Date());

        return departmentDO;
    }

    @Override
    public ServiceResult deleteDepartment(DepartmentDeleteRequest request) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback() {

            @Override
            public void beforeService() {

                AssertUtil.notNull(request.getDepartmentId(), BizErrorCode.PARAM_ILLEGA);
            }

            @Override
            public ServiceResult executeService() {

                return ServiceResult.valueOfSuccess(departmentService.removeById(request.getDepartmentId()));
            }
        });
    }

    private List<DepartmentLoadVO> convert(List<DepartmentDO> departmentDOS) {

        List<DepartmentLoadVO> resultList = new ArrayList<>();

        for (DepartmentDO resource : departmentDOS) {

            DepartmentLoadVO target = new DepartmentLoadVO();

            BeanUtils.copyProperties(resource, target);

            target.setCreateTime(DateUtil.format(resource.getCreateTime(), DateUtil.YYYYMMDDHHMMSS_WHTH_BAR));

            resultList.add(target);
        }

        return resultList;
    }

    /**
     * 获取一个父ID的所有子部门
     *
     * @param parentId
     * @return
     */
    private List<DepartmentTreeVO> getChildDepartment(Integer parentId) {

        List<DepartmentTreeVO> resultList = new ArrayList<>();

        List<DepartmentDO> departmentDOS = departmentService.list(new QueryWrapper<DepartmentDO>().eq("parent_id", parentId));

        for (DepartmentDO departmentDO : departmentDOS) {

            DepartmentTreeVO vo = new DepartmentTreeVO();

            BeanUtils.copyProperties(departmentDO, vo);

            vo.setChirdDepartmentList(getChildDepartment(departmentDO.getId()));

            resultList.add(vo);
        }

        return resultList;
    }

    private void createDepartmentPreCheck(DepartmentCreateRequest request) {

        DepartmentDO departmentDO = departmentService.getById(request.getParentDepartmentId());

        AssertUtil.notNull(departmentDO, BizErrorCode.DEPARTMENT_NOT_EXIST);

        AssertUtil.isFalse(departmentService.count(new QueryWrapper<DepartmentDO>().
                eq("department_name", request.getDepartmentName())) > 0, BizErrorCode.DEPARTMENT_HAS_EXIST);

        if (null != request.getLeaderId()) {
            AssertUtil.notNull(userService.getById(request.getLeaderId()), BizErrorCode.USER_NOT_EXIST);
        }

        ServiceContextHolder.setExt(AdminConstants.PARENT_DEPARTMENT, departmentDO);
    }
}
