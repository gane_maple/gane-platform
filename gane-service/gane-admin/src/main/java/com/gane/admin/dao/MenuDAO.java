package com.gane.admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gane.admin.model.entity.MenuDO;

/**
 * @Description MenuDAO
 * @Date 2020/4/24 7:39
 * @Created by 王弘博
 */
public interface MenuDAO extends BaseMapper<MenuDO> {
}
