package com.gane.admin.model.request.department;

import com.gane.common.template.request.BaseRequest;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


/**
 * @Description TODO
 * @Date 2020/5/10 10:08
 * @Created by 王弘博
 */
@Data
public class DepartmentCreateRequest extends BaseRequest {

    @NotNull
    private Integer parentDepartmentId;

    @NotBlank
    @Size(min = 1, max = 32, message = "departmentName length illegal")
    private String departmentName;

    @NotNull
    private Integer orderNum;

    @NotNull
    private Integer leaderId;

    @NotBlank
    @Pattern(regexp = "^(0|1)$", message = "status format illegal")
    private String status;
}
