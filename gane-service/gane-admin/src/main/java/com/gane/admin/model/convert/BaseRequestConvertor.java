package com.gane.admin.model.convert;

import com.gane.common.template.convert.RequestConvertor;
import com.gane.common.template.convert.RequestConvertorFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * @Description TODO
 * @Date 2020/4/25 9:18
 * @Created by 王弘博
 */
public abstract class BaseRequestConvertor implements RequestConvertor, InitializingBean {

    /**
     * @see InitializingBean#afterPropertiesSet()
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        //注册进去
        RequestConvertorFactory.register(getRequestClass(), this);
    }

    /**
     * 返回转换原始请求报文的类型
     * @return
     */
    protected abstract Class<?> getRequestClass();
}
