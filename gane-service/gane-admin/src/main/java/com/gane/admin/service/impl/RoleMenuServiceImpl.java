package com.gane.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gane.admin.dao.RoleMenuDAO;
import com.gane.admin.model.entity.RoleMenuDO;
import com.gane.admin.service.RoleMenuService;
import org.springframework.stereotype.Service;

/**
 * @Description RoleMenuServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuDAO, RoleMenuDO> implements RoleMenuService {
}
