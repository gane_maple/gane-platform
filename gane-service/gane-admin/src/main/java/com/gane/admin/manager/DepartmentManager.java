package com.gane.admin.manager;

import com.gane.admin.model.request.department.DepartmentCreateRequest;
import com.gane.admin.model.request.department.DepartmentDeleteRequest;
import com.gane.admin.model.request.department.DepartmentLoadRequest;
import com.gane.admin.model.vo.DepartmentLoadVO;
import com.gane.admin.model.vo.DepartmentTreeVO;
import com.gane.common.template.result.ServiceResult;

import java.util.List;

/**
 * @Description TODO
 * @Date 2020/4/24 15:21
 * @Created by 王弘博
 */
public interface DepartmentManager {

    /**
     * queryDepartment
     *
     * @param request
     * @return
     */
    ServiceResult<List<DepartmentLoadVO>> queryDepartment(DepartmentLoadRequest request);

    /**
     * queryDepartmentTree
     *
     * @return
     */
    ServiceResult<List<DepartmentTreeVO>> queryDepartmentTree();

    /**
     * createDepartment
     *
     * @return
     */
    ServiceResult createDepartment(DepartmentCreateRequest request);

    /**
     * deleteDepartment
     * @param request
     * @return
     */
    ServiceResult deleteDepartment(DepartmentDeleteRequest request);
}
