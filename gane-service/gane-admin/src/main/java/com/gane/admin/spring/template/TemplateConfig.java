package com.gane.admin.spring.template;

import com.gane.common.template.service.ServiceTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description TODO
 * @Date 2020/4/27 18:15
 * @Created by 王弘博
 */
@Configuration
public class TemplateConfig {

    @Bean
    public ServiceTemplate serviceTemplate(){
        return new ServiceTemplate();
    }
}
