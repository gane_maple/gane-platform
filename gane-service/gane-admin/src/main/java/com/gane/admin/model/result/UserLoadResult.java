package com.gane.admin.model.result;

import com.gane.admin.model.vo.UserLoadVO;
import com.gane.common.template.result.PageQueryResult;
import lombok.Data;

import java.util.List;

/**
 * @Description TODO
 * @Date 2020/5/10 11:31
 * @Created by 王弘博
 */
@Data
public class UserLoadResult extends PageQueryResult {

    private List<UserLoadVO> resultList;
}
