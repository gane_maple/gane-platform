package com.gane.admin.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gane.admin.dto.PermssionDTO;
import com.gane.admin.dto.RoleDTO;
import com.gane.admin.enums.UserStatusEnum;
import com.gane.admin.manager.RoleAndPermManager;
import com.gane.admin.model.entity.*;
import com.gane.admin.service.*;
import com.gane.common.template.assertion.AssertUtil;
import com.gane.common.template.constant.BizErrorCode;
import com.gane.common.template.result.ServiceResult;
import com.gane.common.template.service.ServiceCallback;
import com.gane.common.template.service.ServiceTemplateImpl;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Description TODO
 * @Date 2020/4/29 19:56
 * @Created by 王弘博
 */
@Service
public class RoleAndPermManagerImpl implements RoleAndPermManager {

    @Resource
    private RoleService roleService;

    @Resource
    private UserService userService;

    @Resource
    private MenuService menuService;

    @Resource
    private RoleMenuService roleMenuService;

    @Resource
    private UserRoleService userRoleService;

    @Override
    public ServiceResult<List<RoleDTO>> queryRolesByUserId(Integer userId) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback<List<RoleDTO>>() {

            @Override
            public void beforeService() {

                AssertUtil.isTrue(null != userId, BizErrorCode.PARAM_ILLEGA);

                UserDO userDO = userService.getById(userId);

                AssertUtil.notNull(null != userDO, BizErrorCode.USER_NOT_EXIST);

                AssertUtil.isTrue(!UserStatusEnum.NORMAL.getCode().equals(userDO.getStatus()), BizErrorCode.USER_IS_CLOSED);
            }

            @Override
            public ServiceResult<List<RoleDTO>> executeService() {

                List<RoleDTO> resultList = new ArrayList<>();

                List<UserRoleDO> userRoleDOS = userRoleService.list(new QueryWrapper<UserRoleDO>().eq("user_id", userId));

                List<Integer> roleIds = new ArrayList<>();

                userRoleDOS.forEach(userRoleDO -> {
                    roleIds.add(userRoleDO.getRoleId());
                });

                if (CollectionUtils.isNotEmpty(roleIds)) {
                    roleCore2Biz(roleService.listByIds(roleIds), resultList);
                }

                return ServiceResult.valueOfSuccess(resultList);
            }
        });
    }

    @Override
    public ServiceResult<List<PermssionDTO>> queryPermsByRoleIds(List<Integer> roleIds) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback<List<PermssionDTO>>() {

            @Override
            public void beforeService() {

                AssertUtil.isTrue(CollectionUtils.isNotEmpty(roleIds), BizErrorCode.PARAM_ILLEGA);

            }

            @Override
            public ServiceResult<List<PermssionDTO>> executeService() {

                List<PermssionDTO> resultList = new ArrayList<>();

                List<RoleMenuDO> roleMenuDOS = roleMenuService.list(new QueryWrapper<RoleMenuDO>().in("role_id", roleIds));

                List<Integer> menuIds = new ArrayList<>();

                roleMenuDOS.stream().forEach(roleMenuDO -> {
                    menuIds.add(roleMenuDO.getMenuId());
                });

                if (CollectionUtils.isNotEmpty(menuIds)) {
                    permssionCore2Biz(menuService.listByIds(menuIds), resultList);
                }

                return ServiceResult.valueOfSuccess(resultList);
            }
        });
    }

    private List<RoleDTO> roleCore2Biz(Collection<RoleDO> roleDOS, List<RoleDTO> resultList) {

        if (CollectionUtils.isEmpty(roleDOS)) {
            return resultList;
        }

        roleDOS.stream().forEach(roleDO -> {

            RoleDTO roleDTO = new RoleDTO();

            BeanUtils.copyProperties(roleDO, roleDTO);

            resultList.add(roleDTO);
        });

        return resultList;
    }

    private List<PermssionDTO> permssionCore2Biz(Collection<MenuDO> menuDOS, List<PermssionDTO> resultList) {

        if (CollectionUtils.isEmpty(menuDOS)) {
            return resultList;
        }

        menuDOS.stream().forEach(menuDO -> {

            PermssionDTO permssionDTO = new PermssionDTO();

            BeanUtils.copyProperties(menuDO, permssionDTO);

            resultList.add(permssionDTO);
        });

        return resultList;
    }
}
