package com.gane.admin.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description UserDTO
 * @Date 2020/4/23 23:20
 * @Created by 王弘博
 */
@Data
@TableName("user")
public class UserDO implements Serializable {

    /**
     * 主鍵ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 部门主键ID
     */
    @TableField("department_id")
    private Integer departmentId;

    /**
     * 登录账户
     */
    @TableField("login_name")
    private String loginName;

    /**
     * 昵称 花名
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 管理员类型（00系统管理员）
     */
    @TableField("user_type")
    private String userType;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 性别（0男 1女 2未知）
     */
    private String sex;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 帐号状态（0正常 1停用）
     */
    private String status;

    /**
     * 头像路径
     */
    private String avatar;

    /**
     * 创建者
     */
    @TableField("create_by")
    private Integer createBy;

    /**
     * 更新者
     */
    @TableField("update_by")
    private Integer updateBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 昵称 花名
     */
    private String remark;
}
