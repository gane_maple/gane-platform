package com.gane.admin.controller;

import com.gane.admin.manager.UserManager;
import com.gane.admin.model.request.UserCreateRequest;
import com.gane.admin.model.request.UserLoadRequest;
import com.gane.admin.model.result.UserLoadResult;
import com.gane.admin.model.vo.UserLoadVO;
import com.gane.admin.spring.redis.RedisClient;
import com.gane.auth.dto.CurrentUser;
import com.gane.common.template.constant.BizScenario;
import com.gane.common.template.constant.Constants;
import com.gane.common.template.request.BaseRequest;
import com.gane.common.template.result.BaseResult;
import com.gane.common.template.result.ServiceResult;
import com.gane.common.template.service.AbstractTemplateServiceImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.List;

/**
 * @Description 用户管理
 * @Date 2020/4/24 7:43
 * @Created by 王弘博
 */
@RestController
@RequestMapping("/user")
public class UserController extends AbstractTemplateServiceImpl {

    @Resource
    private UserManager userManager;

    @PreAuthorize("hasAnyAuthority('user:add')")
    @PostMapping("/add")
    public BaseResult createUser(@RequestBody UserCreateRequest request) {

        return super.bizProcess(BizScenario.USER_CREATE, request, BaseResult.class,

                new FacadeCallback<Void>() {

                    @Override
                    protected void checkParams(BaseRequest request) {

                        super.checkParams(request);
                    }

                    @Override
                    protected ServiceResult<Void> execute(Object... args) {

                        return userManager.createUser((UserCreateRequest) args[0]);
                    }

                    protected <T> BaseResult assembleResult(ServiceResult<T> serviceResult,
                                                            Class<? extends BaseResult> resultClazz) throws Throwable {

                        return super.assembleResult(serviceResult, resultClazz);
                    }
                });
    }

    @PreAuthorize("hasAnyAuthority('user:view')")
    @GetMapping("/load")
    public UserLoadResult loadUser(UserLoadRequest request) {

        return (UserLoadResult) super.bizProcess(BizScenario.USER_LOAD, request, UserLoadResult.class,

                new FacadeCallback<List<UserLoadVO>>() {

                    @Override
                    protected void checkParams(BaseRequest request) {

                        super.checkParams(request);
                    }

                    @Override
                    protected ServiceResult<List<UserLoadVO>> execute(Object... args) {

                        return userManager.queryUser(request);
                    }

                    protected <T> UserLoadResult assembleResult(ServiceResult<T> serviceResult,
                                                                Class<? extends BaseResult> resultClazz) throws Throwable {

                        UserLoadResult result = (UserLoadResult) super.assembleResult(serviceResult, resultClazz);

                        result.setResultList((List<UserLoadVO>) serviceResult.getResultObj());
                        result.setPageNo(request.getPageNo());
                        result.setPageSize(request.getPageSize());
                        result.setPageCount((Long) serviceResult.fetchExtInfoByKey(Constants.PAGE_COUNT));
                        result.setTotalCount((Long) serviceResult.fetchExtInfoByKey(Constants.TOTAL_COUNT));

                        return result;
                    }
                });
    }

    @GetMapping("/hello")
    @PreAuthorize("hasAnyAuthority('hello')")
    public String hello() {
        return "hello";
    }

    @GetMapping("/current")
    public Principal user(Principal principal) {
        return principal;
    }

    @GetMapping("/query")
    @PreAuthorize("hasAnyAuthority('user:view')")
    public String query() {

        CurrentUser currentUser = userManager.currentUser();

        System.out.println(currentUser);

        return "具有user:view权限";
    }

    @Resource
    private RedisClient redisClient;

    @GetMapping("/redis")
    public String redis() {

        redisClient.set("name", "王弘博");

        String name = (String) redisClient.get("name");

        return name;
    }


}
