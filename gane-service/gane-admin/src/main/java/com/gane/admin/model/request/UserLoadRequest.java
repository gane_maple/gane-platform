package com.gane.admin.model.request;

import com.gane.common.template.request.BaseRequest;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * @Description TODO
 * @Date 2020/5/10 10:08
 * @Created by 王弘博
 */
@Data
public class UserLoadRequest extends BaseRequest {

    private Integer departmentId;

    private String loginName;

    private String mobile;

    private String status;

    private String startCreateTime;

    private String endCreateTime;

    @NotNull
    private Integer pageNo = 1;

    @NotNull
    private Integer pageSize = 10;
}
