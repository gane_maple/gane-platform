package com.gane.admin.controller;

import com.gane.admin.manager.DepartmentManager;
import com.gane.admin.model.request.NoConditionRequest;
import com.gane.admin.model.request.department.DepartmentCreateRequest;
import com.gane.admin.model.request.department.DepartmentDeleteRequest;
import com.gane.admin.model.request.department.DepartmentLoadRequest;
import com.gane.admin.model.result.DepartmentLoadResult;
import com.gane.admin.model.result.DepartmentTreeResult;
import com.gane.admin.model.vo.DepartmentLoadVO;
import com.gane.admin.model.vo.DepartmentTreeVO;
import com.gane.common.template.constant.BizScenario;
import com.gane.common.template.request.BaseRequest;
import com.gane.common.template.result.BaseResult;
import com.gane.common.template.result.ServiceResult;
import com.gane.common.template.service.AbstractTemplateServiceImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description 用户管理
 * @Date 2020/4/24 7:43
 * @Created by 王弘博
 */
@RestController
@RequestMapping("/department")
public class DepartmentController extends AbstractTemplateServiceImpl {

    @Resource
    private DepartmentManager departmentManager;

    @PreAuthorize("hasAnyAuthority('department:view')")
    @GetMapping("/load")
    public DepartmentLoadResult loadDepartment(DepartmentLoadRequest request) {

        return (DepartmentLoadResult) super.bizProcess(BizScenario.DEPARTMENT_LOAD, request, DepartmentLoadResult.class,

                new FacadeCallback<List<DepartmentLoadVO>>() {

                    @Override
                    protected void checkParams(BaseRequest request) {

                        super.checkParams(request);
                    }

                    @Override
                    protected ServiceResult<List<DepartmentLoadVO>> execute(Object... args) {

                        return departmentManager.queryDepartment(request);
                    }

                    protected <T> DepartmentLoadResult assembleResult(ServiceResult<T> serviceResult,
                                                                      Class<? extends BaseResult> resultClazz) throws Throwable {

                        DepartmentLoadResult result = (DepartmentLoadResult) super.assembleResult(serviceResult, resultClazz);

                        result.setResultList((List<DepartmentLoadVO>) serviceResult.getResultObj());

                        return result;
                    }
                });
    }

    @PreAuthorize("hasAnyAuthority('department:tree')")
    @GetMapping("/tree")
    public DepartmentTreeResult tree(NoConditionRequest request) {

        return (DepartmentTreeResult) super.bizProcess(BizScenario.DEPARTMENT_TREE, request, DepartmentTreeResult.class,

                new FacadeCallback<List<DepartmentTreeVO>>() {

                    @Override
                    protected ServiceResult<List<DepartmentTreeVO>> execute(Object... args) {

                        return departmentManager.queryDepartmentTree();
                    }

                    protected <T> DepartmentTreeResult assembleResult(ServiceResult<T> serviceResult,
                                                                      Class<? extends BaseResult> resultClazz) throws Throwable {

                        DepartmentTreeResult result = (DepartmentTreeResult) super.assembleResult(serviceResult, resultClazz);

                        result.setResultList((List<DepartmentTreeVO>) serviceResult.getResultObj());

                        return result;
                    }
                });
    }

    @PreAuthorize("hasAnyAuthority('department:add')")
    @PostMapping("/add")
    public BaseResult createDepartment(@RequestBody DepartmentCreateRequest request) {

        return super.bizProcess(BizScenario.DEPARTMENT_CREATE, request, BaseResult.class,

                new FacadeCallback<Void>() {

                    @Override
                    protected void checkParams(BaseRequest request) {

                        super.checkParams(request);
                    }

                    @Override
                    protected ServiceResult<Void> execute(Object... args) {

                        return departmentManager.createDepartment(request);
                    }

                    protected <T> BaseResult assembleResult(ServiceResult<T> serviceResult,
                                                            Class<? extends BaseResult> resultClazz) throws Throwable {

                        return super.assembleResult(serviceResult, resultClazz);
                    }
                });
    }

    @PreAuthorize("hasAnyAuthority('department:delete')")
    @PostMapping("/delete")
    public BaseResult deleteDepartment(DepartmentDeleteRequest request) {

        return super.bizProcess(BizScenario.DEPARTMENT_DELETE, request, BaseResult.class,

                new FacadeCallback<Void>() {

                    @Override
                    protected void checkParams(BaseRequest request) {

                        super.checkParams(request);
                    }

                    @Override
                    protected ServiceResult<Void> execute(Object... args) {

                        return departmentManager.deleteDepartment(request);
                    }

                    protected <T> BaseResult assembleResult(ServiceResult<T> serviceResult,
                                                            Class<? extends BaseResult> resultClazz) throws Throwable {

                        return super.assembleResult(serviceResult, resultClazz);
                    }
                });
    }

}
