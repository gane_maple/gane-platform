package com.gane.admin.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description MenuDO
 * @Date 2020/4/23 23:20
 * @Created by 王弘博
 */
@Data
@TableName("menu")
public class MenuDO implements Serializable {

    /**
     * 主鍵ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 菜单名称
     */
    @TableField("menu_name")
    private String menuName;

    /**
     * 菜单标识
     */
    @TableField("menu_key")
    private String menuKey;

    /**
     * 祖级列表
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 显示顺序
     */
    @TableField("order_num")
    private Integer orderNum;

    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    @TableField("menu_type")
    private String menuType;

    /**
     * 权限标识
     */
    private String perms;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 重定向
     */
    private String redirect;

    /**
     * 创建者
     */
    @TableField("create_by")
    private Integer createBy;

    /**
     * 更新者
     */
    @TableField("update_by")
    private Integer updateBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    private String remark;
}
