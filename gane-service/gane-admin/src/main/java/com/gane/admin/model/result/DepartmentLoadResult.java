package com.gane.admin.model.result;

import com.gane.admin.model.vo.DepartmentLoadVO;
import com.gane.common.template.result.BaseResult;
import lombok.Data;

import java.util.List;

/**
 * @Description TODO
 * @Date 2020/5/10 11:31
 * @Created by 王弘博
 */
@Data
public class DepartmentLoadResult extends BaseResult {

    private List<DepartmentLoadVO> resultList;
}
