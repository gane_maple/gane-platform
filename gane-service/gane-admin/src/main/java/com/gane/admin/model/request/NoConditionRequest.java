package com.gane.admin.model.request;

import com.gane.common.template.request.BaseRequest;
import lombok.Data;


/**
 * @Description TODO
 * @Date 2020/5/10 10:08
 * @Created by 王弘博
 */
@Data
public class NoConditionRequest extends BaseRequest {

}
