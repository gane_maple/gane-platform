package com.gane.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gane.admin.dao.MenuDAO;
import com.gane.admin.model.entity.MenuDO;
import com.gane.admin.service.MenuService;
import org.springframework.stereotype.Service;

/**
 * @Description MenuServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuDAO, MenuDO> implements MenuService {
}
