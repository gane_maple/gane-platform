package com.gane.admin.model.request;

import com.gane.common.template.request.BaseRequest;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * @Description TODO
 * @Date 2020/4/24 12:25
 * @Created by 王弘博
 */
@Data
public class UserCreateRequest extends BaseRequest {

    @NotNull(message = "departmentId cant be null")
    private Integer departmentId;

    @NotBlank(message = "loginName cant be blank")
    @Size(min = 1, max = 32, message = "loginName length illegal")
    private String loginName;

    @NotBlank(message = "nickName cant be blank")
    @Size(min = 1, max = 32, message = "nickName length illegal")
    private String nickName;

    @NotBlank(message = "userType cant be blank")
    @Pattern(regexp = "^(00|01)$", message = "userType format illegal")
    private String userType;

    @NotBlank(message = "email cant be blank")
    @Email(message = "email format illegal")
    private String email;

    @NotBlank(message = "mobile cant be blank")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "mobile format illegal")
    private String mobile;

    @NotBlank(message = "sex cant be blank")
    @Pattern(regexp = "^(0|1)$", message = "sex format illegal")
    private String sex;

    @NotBlank(message = "password cant be blank")
    @Size(min = 1, max = 16, message = "password length illegal")
    private String password;

    @NotBlank(message = "status cant be blank")
    @Pattern(regexp = "^(0|1)$", message = "status format illegal")
    private String status;

    @Size(max = 128, message = "password length illegal")
    private String avatar;

    @Size(max = 500, message = "remark length illegal")
    private String remark;
}
