package com.gane.admin.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description RoleDO
 * @Date 2020/4/23 23:20
 * @Created by 王弘博
 */
@Data
@TableName("role")
public class RoleDO implements Serializable {

    /**
     * 主鍵ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 角色名称
     */
    @TableField("role_name")
    private String roleName;

    /**
     * 角色标识
     */
    @TableField("role_key")
    private String roleKey;

    /**
     * 显示顺序
     */
    @TableField("order_num")
    private Integer orderNum;

    /**
     * 数据范围（1：全部数据权限 2：自定数据权限）
     */
    @TableField("data_scope")
    private String dataScope;

    /**
     * 部门状态（0正常 1停用）
     */
    private String status;

    /**
     * 创建者
     */
    @TableField("create_by")
    private Integer createBy;

    /**
     * 更新者
     */
    @TableField("update_by")
    private Integer updateBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    private String remark;
}
