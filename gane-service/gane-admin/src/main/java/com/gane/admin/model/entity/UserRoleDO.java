package com.gane.admin.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description UserRoleDO
 * @Date 2020/4/23 23:20
 * @Created by 王弘博
 */
@Data
@TableName("user_role")
public class UserRoleDO implements Serializable {

    /**
     * 管理员主键ID
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 角色主键ID
     */
    @TableField("role_id")
    private Integer roleId;
}
