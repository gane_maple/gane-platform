package com.gane.admin.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description TODO
 * @Date 2020/5/10 11:06
 * @Created by 王弘博
 */
@Data
public class UserLoadVO implements Serializable {

    private Integer userId;

    private String loginName;

    private String nickName;

    private String departmentName;

    private String mobile;

    private String status;

    private String createTime;
}
