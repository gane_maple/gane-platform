package com.gane.admin.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description TODO
 * @Date 2020/5/21 11:06
 * @Created by 王弘博
 */
@Data
public class DepartmentTreeVO implements Serializable {

    private Integer id;

    private String departmentName;

    private Integer orderNum;

    private List<DepartmentTreeVO> chirdDepartmentList;
}
