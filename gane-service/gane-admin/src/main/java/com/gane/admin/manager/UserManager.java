package com.gane.admin.manager;

import com.gane.admin.dto.UserDTO;
import com.gane.admin.model.request.UserCreateRequest;
import com.gane.admin.model.request.UserLoadRequest;
import com.gane.admin.model.vo.UserLoadVO;
import com.gane.auth.dto.CurrentUser;
import com.gane.common.template.result.ServiceResult;

import java.util.List;

/**
 * @Description TODO
 * @Date 2020/4/24 15:21
 * @Created by 王弘博
 */
public interface UserManager {

    /**
     * createUser
     * @param request
     * @return
     */
    ServiceResult createUser(UserCreateRequest request);

    /**
     *
     * @param loginName
     * @return
     */
    ServiceResult<UserDTO> queryByLoginName(String loginName);

    /**
     * 获取当前登录的用户
     * @return
     */
    CurrentUser currentUser();

    /**
     * queryUser
     * @return
     */
    ServiceResult<List<UserLoadVO>> queryUser(UserLoadRequest request);
}
