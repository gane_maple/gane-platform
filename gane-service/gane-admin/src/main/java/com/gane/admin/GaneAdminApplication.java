package com.gane.admin;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@EnableDistributedTransaction
@EnableFeignClients("com.gane.*")
@MapperScan("com.gane.admin.dao")
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@EnableDiscoveryClient
@SpringBootApplication
public class GaneAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(GaneAdminApplication.class, args);
        System.out.println("----------gane-admin启动成功！");
    }

}
