package com.gane.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gane.admin.model.entity.DepartmentDO;

/**
 * @Description DepartmentService
 * @Date 2020/4/24 7:40
 * @Created by 王弘博
 */
public interface DepartmentService extends IService<DepartmentDO> {
}
