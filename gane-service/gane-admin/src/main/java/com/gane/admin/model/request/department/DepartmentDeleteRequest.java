package com.gane.admin.model.request.department;

import com.gane.common.template.request.BaseRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;


/**
 * @Description TODO
 * @Date 2020/5/10 10:08
 * @Created by 王弘博
 */
@Data
public class DepartmentDeleteRequest extends BaseRequest {

    @NotNull
    private Integer departmentId;

}
