//package com.gane.admin.spring.swagger;

/*
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.*;*/

/**
 * @Description SwaggerConfig
 * @Date 2020/4/26 15:20
 * @Created by 王弘博
 */
//@EnableSwaggerBootstrapUI
/*@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.gane.admin.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .securitySchemes(Collections.singletonList(securityScheme()))
                .securityContexts(Collections.singletonList(securityContext()));
    }

    *//**
     * 这个方法主要是写一些文档的描述
     *//*
    private ApiInfo apiInfo() {
        return new ApiInfo(
                "某某系统API",
                "This is a very pretty document!",
                "1.0",
                "",
                new Contact("师父领进门", "", "qixiazhen@qq.com"),
                "", "", Collections.emptyList());
    }

    *//**
     * 这个类决定了你使用哪种认证方式，我这里使用密码模式
     * 其他方式自己摸索一下，完全莫问题啊
     *//*
    private SecurityScheme securityScheme() {
        GrantType grantType = new ResourceOwnerPasswordCredentialsGrant("http://gane-eureka1:11004/gane-auth" + "/oauth/token");

        return new OAuthBuilder()
                .name("spring_oauth")
                .grantTypes(Collections.singletonList(grantType))
                .scopes(Arrays.asList(scopes()))
                .build();
    }

    *//**
     * 这里设置 swagger2 认证的安全上下文
     *//*
    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(Collections.singletonList(new SecurityReference("spring_oauth", scopes())))
                .forPaths(PathSelectors.any())
                .build();
    }

    *//**
     * 这里是写允许认证的scope
     *//*
    private AuthorizationScope[] scopes() {
        return new AuthorizationScope[]{
                new AuthorizationScope("all", "All scope is trusted!")
        };
    }

    @Bean
    public SecurityConfiguration securityInfo() {
        return new SecurityConfiguration("android", "android", "read","swagger",
                ",", null, true);
    }*/

    /**
     * Api分组，可以定义多个组
     */
    /*@Bean
    public Docket jackApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("jack")
                .genericModelSubstitutes(DeferredResult.class)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .pathMapping("/")
                .select()
                //                .apis(RequestHandlerSelectors.basePackage("net.jack.lt"))
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .apis(RequestHandlerSelectors.basePackage("com.gane.admin.controller"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(new ArrayList(Collections.singleton((oauth()))))
                .securityContexts(new ArrayList(Collections.singleton(securityContext())))
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("swagger")
                .description("详情描述")
                .termsOfServiceUrl("termsOfServiceUrl")
                .contact("王弘博")
                .version("1.0")
                .build();
        return apiInfo;
    }

    @Bean
    SecurityScheme apiKey() {
        return new ApiKey("swagger", "平台在线API", "header");
    }

    @Bean
    SecurityContext securityContext() {
        AuthorizationScope[] scopes = new AuthorizationScope[]{new AuthorizationScope("read", "read")};
        SecurityReference securityReference = SecurityReference
                .builder()
                .reference("oauth2")
                .scopes(scopes)
                .build();

        return SecurityContext
                .builder()
                .securityReferences(new ArrayList(Collections.singleton(securityReference)))
                .forPaths(PathSelectors.any())//ant("/api/**") // 最好是能够统一前缀
                .build();
    }

    @Bean
    SecurityScheme oauth() {
        return new OAuthBuilder()
                .name("oauth2")
                .grantTypes(grantTypes())
                .scopes(scopes())
                .build();
    }

    List<AuthorizationScope> scopes() {
        return new ArrayList(Collections.singleton(new AuthorizationScope("read", "read")));
    }

    List<GrantType> grantTypes() {
        *//*List<GrantType> grants = new ArrayList(Collections.singleton(new AuthorizationCodeGrant(
                new TokenRequestEndpoint("http://gane-eureka1:11004/gane-auth/oauth/authorize", "android", "android"),
                new TokenEndpoint("http://gane-eureka1:11004/gane-auth/oauth/token", "access_token"))));*//*

        GrantType grantType = new ResourceOwnerPasswordCredentialsGrant("http://gane-eureka1:11004/gane-auth" + "/oauth/token");
        return new ArrayList(Collections.singleton(grantType));
    }

    @Bean
    public SecurityConfiguration securityInfo() {
        return new SecurityConfiguration("android", "android", "read",
                "swagger", "平台在线API", ApiKeyVehicle.HEADER, "", ",");
    }*/
//}
