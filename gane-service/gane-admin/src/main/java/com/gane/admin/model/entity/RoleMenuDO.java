package com.gane.admin.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description RoleMenuDO
 * @Date 2020/4/23 23:20
 * @Created by 王弘博
 */
@Data
@TableName("role_menu")
public class RoleMenuDO implements Serializable {

    /**
     * 角色主键ID
     */
    @TableField("role_id")
    private Integer roleId;

    /**
     * 菜单主键ID
     */
    @TableField("menu_id")
    private Integer menuId;
}
