package com.gane.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gane.admin.dao.RoleDepartmentDAO;
import com.gane.admin.model.entity.RoleDepartmentDO;
import com.gane.admin.service.RoleDepartmentService;
import org.springframework.stereotype.Service;

/**
 * @Description RoleDepartmentServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class RoleDepartmentServiceImpl extends ServiceImpl<RoleDepartmentDAO, RoleDepartmentDO> implements RoleDepartmentService {
}
