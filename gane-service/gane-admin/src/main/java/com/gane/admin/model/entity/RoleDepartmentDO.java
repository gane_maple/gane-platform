package com.gane.admin.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description RoleDepartmentDO
 * @Date 2020/4/23 23:20
 * @Created by 王弘博
 */
@Data
@TableName("role_department")
public class RoleDepartmentDO implements Serializable {

    /**
     * 角色主键ID
     */
    @TableField("role_id")
    private Integer roleId;

    /**
     * 部门主键ID
     */
    @TableField("department_id")
    private Integer departmentId;
}
