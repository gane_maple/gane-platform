package com.gane.admin.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gane.admin.dto.PermssionDTO;
import com.gane.admin.dto.RoleDTO;
import com.gane.admin.dto.UserDTO;
import com.gane.admin.enums.UserStatusEnum;
import com.gane.admin.manager.UserManager;
import com.gane.admin.model.entity.*;
import com.gane.admin.model.request.UserCreateRequest;
import com.gane.admin.model.request.UserLoadRequest;
import com.gane.admin.model.vo.UserLoadVO;
import com.gane.admin.service.*;
import com.gane.auth.dto.CurrentUser;
import com.gane.common.template.assertion.AssertUtil;
import com.gane.common.template.constant.BizErrorCode;
import com.gane.common.template.constant.Constants;
import com.gane.common.template.context.ServiceContextHolder;
import com.gane.common.template.result.ServiceResult;
import com.gane.common.template.service.ServiceCallback;
import com.gane.common.template.service.ServiceTemplateImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description TODO
 * @Date 2020/4/24 15:23
 * @Created by 王弘博
 */
@Service
public class UserManagerImpl implements UserManager {

    @Resource
    private RoleService roleService;

    @Resource
    private UserService userService;

    @Resource
    private UserRoleService userRoleService;

    @Resource
    private MenuService menuService;

    @Resource
    private RoleMenuService roleMenuService;

    @Override
    public ServiceResult createUser(UserCreateRequest request) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback() {

            @Override
            public void beforeService() {

                createUserPreCheck(request);
            }

            @Override
            public ServiceResult executeService() {

                boolean save = userService.save(covert(request));

                return ServiceResult.valueOfSuccess(save);
            }
        });
    }

    @Override
    public ServiceResult<UserDTO> queryByLoginName(String loginName) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback<UserDTO>() {

            @Override
            public void beforeService() {

                AssertUtil.isTrue(StringUtils.isNotBlank(loginName), BizErrorCode.PARAM_ILLEGA);
            }

            @Override
            public ServiceResult<UserDTO> executeService() {

                UserDO userDO = userService.getOne(new QueryWrapper<UserDO>().eq("login_name", loginName));

                if (null == userDO) {

                    return ServiceResult.valueOfSuccess();
                }

                UserDTO userDTO = new UserDTO();

                BeanUtils.copyProperties(userDO, userDTO);

                if (UserStatusEnum.NORMAL.getCode().equals(userDO.getStatus())) {

                    userDTO.setRoleDTOS(loadRoles(userDO.getId()));
                }

                return ServiceResult.valueOfSuccess(userDTO);
            }
        });
    }

    @Override
    public CurrentUser currentUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AssertUtil.isTrue(authentication.isAuthenticated(), BizErrorCode.UNAUTHORIZED);

        CurrentUser currentUser = new CurrentUser();

        String loginName = (String) authentication.getPrincipal();

        UserDO userDO = userService.getOne(new QueryWrapper<UserDO>().eq("login_name", loginName).eq("status", 0));

        AssertUtil.notNull(userDO, BizErrorCode.USER_NOT_EXIST, "user not exist, loginName = " + loginName);

        currentUser.setUserId(userDO.getId());

        currentUser.setLoginName(loginName);

        currentUser.setDepartmentId(userDO.getDepartmentId());

        currentUser.setAuthorities(authentication.getAuthorities().stream().map(authority -> {
            return authority.getAuthority();
        }).collect(Collectors.toList()));

        return currentUser;
    }

    @Override
    public ServiceResult<List<UserLoadVO>> queryUser(UserLoadRequest request) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback<List<UserLoadVO>>() {

            @Override
            public ServiceResult<List<UserLoadVO>> executeService() {

                Page page = new Page(request.getPageNo(), request.getPageSize());

                QueryWrapper<UserDO> queryWrapper = new QueryWrapper<>();

                if (null != request.getDepartmentId()) {
                    queryWrapper.eq("department_id", request.getDepartmentId());
                }

                if (StringUtils.isNotBlank(request.getLoginName())) {
                    queryWrapper.like("login_name", request.getLoginName());
                }

                if (StringUtils.isNotBlank(request.getMobile())) {
                    queryWrapper.like("mobile", request.getMobile());
                }

                if (StringUtils.isNotBlank(request.getStatus())) {
                    queryWrapper.eq("status", request.getStatus());
                }

                if (StringUtils.isNotBlank(request.getStartCreateTime()) && StringUtils.isNotBlank(request.getEndCreateTime())) {
                    queryWrapper.gt("create_time", request.getStartCreateTime());
                    queryWrapper.lt("create_time", request.getEndCreateTime());
                }

                IPage<UserDO> pageResult = userService.page(page, queryWrapper);

                if (null == pageResult) {

                    return ServiceResult.valueOfSuccess();
                }

                List<UserLoadVO> resultList = new ArrayList<>();

                List<UserDO> userDOs = pageResult.getRecords();

                if (CollectionUtils.isEmpty(userDOs)) {
                    return ServiceResult.valueOfSuccess();
                }

                for (UserDO userDO : userDOs) {
                    UserLoadVO vo = new UserLoadVO();
                    BeanUtils.copyProperties(userDO, vo);
                    resultList.add(vo);
                }

                ServiceResult<List<UserLoadVO>> serviceResult = new ServiceResult<>();

                serviceResult.setResultObj(resultList);

                Map<String, Object> extInfoMap = new HashMap<>();

                extInfoMap.put(Constants.PAGE_COUNT, pageResult.getPages());
                extInfoMap.put(Constants.TOTAL_COUNT, pageResult.getTotal());

                serviceResult.setExtInfoMap(extInfoMap);

                return serviceResult;
            }
        });
    }

    private void createUserPreCheck(UserCreateRequest request) {

        AssertUtil.isFalse(userService.count(new QueryWrapper<UserDO>().eq("login_name", request.getLoginName())) > 0,
                BizErrorCode.USER_LOGIN_NAME_HAS_EXIST);

        AssertUtil.isFalse(userService.count(new QueryWrapper<UserDO>().eq("nick_name", request.getNickName())) > 0,
                BizErrorCode.USER_NICK_NAME_HAS_EXIST);

        AssertUtil.isFalse(userService.count(new QueryWrapper<UserDO>().eq("email", request.getEmail())) > 0,
                BizErrorCode.USER_EMAIL_HAS_EXIST);

        AssertUtil.isFalse(userService.count(new QueryWrapper<UserDO>().eq("mobile", request.getMobile())) > 0,
                BizErrorCode.USER_MOBILE_HAS_EXIST);

    }

    private UserDO covert(UserCreateRequest request) {

        UserDO userDO = new UserDO();

        userDO.setDepartmentId(request.getDepartmentId());
        userDO.setLoginName(request.getLoginName());
        userDO.setNickName(request.getNickName());
        userDO.setUserType(request.getUserType());
        userDO.setEmail(request.getEmail());
        userDO.setMobile(request.getMobile());
        userDO.setSex(request.getSex());
        userDO.setPassword(new BCryptPasswordEncoder().encode(request.getPassword()));
        userDO.setAvatar(request.getAvatar());
        userDO.setCreateBy(currentUser().getUserId());
        userDO.setUpdateBy(currentUser().getUserId());
        userDO.setCreateTime(ServiceContextHolder.getCurrentTime());
        userDO.setUpdateTime(new Date());

        return userDO;
    }

    private List<RoleDTO> loadRoles(Integer userId) {

        List<RoleDTO> roleDTOS = new ArrayList<>();

        List<UserRoleDO> userRoleDOS = userRoleService.list(new QueryWrapper<UserRoleDO>().eq("user_id", userId));

        for (UserRoleDO userRoleDO : userRoleDOS) {

            RoleDO roleDO = roleService.getById(userRoleDO.getRoleId());

            RoleDTO roleDTO = new RoleDTO();

            BeanUtils.copyProperties(roleDO, roleDTO);

            roleDTO.setPermssionDTOS(loadPerms(roleDO.getId()));

            roleDTOS.add(roleDTO);
        }

        return roleDTOS;

    }

    private List<PermssionDTO> loadPerms(Integer roleId) {

        List<PermssionDTO> resultList = new ArrayList<>();

        List<RoleMenuDO> roleMenuDOS = roleMenuService.list(new QueryWrapper<RoleMenuDO>().eq("role_id", roleId));

        List<Integer> menuIds = new ArrayList<>();

        roleMenuDOS.stream().forEach(roleMenuDO -> {
            menuIds.add(roleMenuDO.getMenuId());
        });

        if (CollectionUtils.isNotEmpty(menuIds)) {
            permssionCore2Biz(menuService.listByIds(menuIds), resultList);
        }

        return resultList;

    }

    private List<RoleDTO> roleCore2Biz(Collection<RoleDO> roleDOS, List<com.gane.admin.dto.RoleDTO> resultList) {

        if (CollectionUtils.isEmpty(roleDOS)) {
            return resultList;
        }

        roleDOS.stream().forEach(roleDO -> {

            RoleDTO roleDTO = new RoleDTO();

            BeanUtils.copyProperties(roleDO, roleDTO);

            resultList.add(roleDTO);
        });

        return resultList;
    }

    private List<PermssionDTO> permssionCore2Biz(Collection<MenuDO> menuDOS, List<PermssionDTO> resultList) {

        if (CollectionUtils.isEmpty(menuDOS)) {
            return resultList;
        }

        menuDOS.stream().forEach(menuDO -> {

            PermssionDTO permssionDTO = new PermssionDTO();

            BeanUtils.copyProperties(menuDO, permssionDTO);

            resultList.add(permssionDTO);
        });

        return resultList;
    }
}
