package com.gane.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gane.admin.dao.DepartmentDAO;
import com.gane.admin.model.entity.DepartmentDO;
import com.gane.admin.service.DepartmentService;
import org.springframework.stereotype.Service;

/**
 * @Description DepartmentServiceImpl
 * @Date 2020/4/24 7:41
 * @Created by 王弘博
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentDAO, DepartmentDO> implements DepartmentService {
}
