package com.gane.auth.feign.client;

import com.gane.admin.dto.PermssionDTO;
import com.gane.admin.dto.RoleDTO;
import com.gane.admin.dto.UserDTO;

import java.util.List;

/**
 * @Description TODO
 * @Date 2020/4/27 17:08
 * @Created by 王弘博
 */
public interface UserClient {

    UserDTO queryByLoginName(String loginName);

    List<RoleDTO> queryRolesByUserId(Integer userId);

    List<PermssionDTO> queryPermsByRoleIds(List<Integer> roleIds);
}
