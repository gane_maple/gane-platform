/*
package com.gane.auth.spring.shiro;

import com.gane.admin.dto.PermssionDTO;
import com.gane.admin.dto.RoleDTO;
import com.gane.admin.dto.UserDTO;
import com.gane.admin.enums.UserStatusEnum;
import com.gane.auth.dto.CurrentUser;
import com.gane.auth.feign.client.UserClient;
import com.gane.common.template.constant.BizErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.BeanUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

*/
/**
 * @Description 自定义realm，包括认证和授权
 * @Date 2019/7/11 21:41
 * @Created by 王弘博
 *//*

@Slf4j
public class UserShiroRealm extends AuthorizingRealm {

    @Resource
    private UserClient userClient;

    */
/**
     * 授权
     *
     * @param principal
     * @return
     *//*

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {

        CurrentUser shiroUser = (CurrentUser) principal.getPrimaryPrincipal();

        List<RoleDTO> roleDTOS = userClient.queryRolesByUserId(shiroUser.getId());

        List<Integer> roleIds = new ArrayList<>();

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();

        roleDTOS.stream().forEach(roleDTO -> {
            authorizationInfo.addRole(roleDTO.getRoleKey());
            roleIds.add(roleDTO.getId());
        });

        List<PermssionDTO> permssionDTOS = userClient.queryPermsByRoleIds(roleIds);

        permssionDTOS.stream().filter(permssionDTO -> StringUtils.isNotBlank(permssionDTO.getPerms()))
                .forEach(permssionDTO -> {
                    authorizationInfo.addStringPermission(permssionDTO.getPerms());
                });

        return authorizationInfo;
    }

    */
/**
     * 认证
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     *//*

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        String loginName = (String) authenticationToken.getPrincipal();

        UserDTO userDTO = userClient.queryByLoginName(loginName);

        if (null == userDTO) {

            throw new AccountException(BizErrorCode.USER_NOT_EXIST.getResultMsg());
        }

        if (!UserStatusEnum.NORMAL.getCode().equals(userDTO.getStatus())) {

            throw new LockedAccountException(BizErrorCode.USER_IS_CLOSED.getResultMsg());
        }

        CurrentUser shiroUser = new CurrentUser();

        BeanUtils.copyProperties(userDTO, shiroUser);

        return new SimpleAuthenticationInfo(shiroUser, userDTO.getPassword(), ByteSource.Util.bytes(userDTO.getSalt()), getName());
    }
}
*/
