package com.gane.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients("com.gane.*")
@EnableDiscoveryClient
@SpringBootApplication
public class GaneAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(GaneAuthApplication.class, args);
        System.out.println("----------gane-auth启动成功！");
    }

}
