/*
package com.gane.auth.spring.shiro;

import org.apache.shiro.web.filter.authc.PassThruAuthenticationFilter;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

*/
/**
 * @Description 解决 shiro 里 OPTIONS 问题  让权限认证控制跳过OPTIONS方法，不让他做认证检查。
 * @Date 2019/8/30 17:42
 * @Created by 王弘博
 *//*

public class MyPassThruAuthenticationFilter extends PassThruAuthenticationFilter {

    @Override
    public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {

        HttpServletRequest req = (HttpServletRequest) request;
        if (req.getMethod().equals(RequestMethod.OPTIONS.name())) {
            return true;
        }
        return super.onPreHandle(request, response, mappedValue);
    }
}
*/
