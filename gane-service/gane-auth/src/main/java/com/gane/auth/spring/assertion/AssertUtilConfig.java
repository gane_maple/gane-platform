package com.gane.auth.spring.assertion;

import com.gane.common.template.assertion.AssertUtil;
import com.gane.common.template.exception.BizException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description TODO
 * @Date 2020/4/24 21:43
 * @Created by 王弘博
 */
@Configuration
public class AssertUtilConfig {

    @Bean
    public AssertUtil assertUtil() {
        AssertUtil assertUtil = new AssertUtil();
        assertUtil.setExceptionClassName(BizException.class.getCanonicalName());
        return assertUtil;
    }
}
