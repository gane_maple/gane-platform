package com.gane.auth.controller;

import com.gane.common.template.constant.BizErrorCode;
import com.gane.common.template.result.BaseResult;
import com.gane.common.template.result.ServiceResult;
import com.gane.common.template.service.AbstractTemplateServiceImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;

/**
 * @Description 认证管理
 * @Date 2020/4/24 7:43
 * @Created by 王弘博
 */
@RestController
@RequestMapping("/auth")
public class AuthController extends AbstractTemplateServiceImpl {


    @Resource
    private ConsumerTokenServices consumerTokenServices;

    @GetMapping("/member")
    public Principal user(Principal member) {

        System.out.println(member);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        System.out.println(authentication);

        return member;
    }

    @GetMapping("/unauth")
    public BaseResult unauth() {

        return ServiceResult.valueOfFail(BizErrorCode.UNAUTHORIZED);
    }

    @PostMapping("/exit")
    public boolean exit(String access_token){
        return consumerTokenServices.revokeToken(access_token);
    }

    @GetMapping("/testAuth")
    public String testAuth() {

        System.out.println("测试未登录时，可以进来该接口不");

        return "OK";
    }


    //@RequiresPermissions("user:view")
    @GetMapping("/testPerms")
    public String testPerms() {

        System.out.println("测试权限");

        return "测试权限OK";
    }

    //@RequiresRoles("user")
    @GetMapping("/testRole")
    public String testRole() {

        System.out.println("测试角色");

        return "测试角色OK";
    }
}
