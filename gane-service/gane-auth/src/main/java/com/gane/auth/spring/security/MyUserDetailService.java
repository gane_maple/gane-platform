package com.gane.auth.spring.security;

import com.gane.admin.dto.UserDTO;
import com.gane.auth.feign.client.UserClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

/**
 * @Description TODO
 * @Date 2020/5/1 12:28
 * @Created by 王弘博
 */
@Service("userDetailService")
public class MyUserDetailService implements UserDetailsService {

    @Resource
    private UserClient userClient;

    @Override
    public UserDetails loadUserByUsername(String loginName) throws UsernameNotFoundException {

        UserDTO userDTO = userClient.queryByLoginName(loginName);

        if (null == userDTO) {
            throw new UsernameNotFoundException(loginName);
        }

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        // 可用性 :true:可用 false:不可用
        boolean enabled = true;
        // 过期性 :true:没过期 false:过期
        boolean accountNonExpired = true;
        // 有效性 :true:凭证有效 false:凭证无效
        boolean credentialsNonExpired = true;
        // 锁定性 :true:未锁定 false:已锁定
        boolean accountNonLocked = true;

        userDTO.getRoleDTOS().forEach(roleDTO -> {

            //角色必须是ROLE_开头，可以在数据库中设置
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(roleDTO.getRoleName());

            grantedAuthorities.add(grantedAuthority);

            roleDTO.getPermssionDTOS().stream()

                    .filter(permssionDTO -> StringUtils.isNotBlank(permssionDTO.getPerms()))

                    .forEach(permssionDTO -> {

                        GrantedAuthority authority = new SimpleGrantedAuthority(permssionDTO.getPerms());

                        grantedAuthorities.add(authority);
                    });
        });

        return new User(userDTO.getLoginName(), userDTO.getPassword(),
                enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, grantedAuthorities);
    }
}
