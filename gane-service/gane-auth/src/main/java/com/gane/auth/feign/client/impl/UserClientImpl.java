package com.gane.auth.feign.client.impl;

import com.gane.admin.dto.PermssionDTO;
import com.gane.admin.dto.RoleDTO;
import com.gane.admin.dto.UserDTO;
import com.gane.admin.feign.IUserFeignClient;
import com.gane.auth.feign.client.UserClient;
import com.gane.common.template.assertion.ServiceChecker;
import com.gane.common.template.result.ServiceResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description 包装 gane-admin 暴露的接口
 * @Date 2020/4/27 17:08
 * @Created by 王弘博
 */
@Service
public class UserClientImpl implements UserClient {

    @Resource
    private IUserFeignClient userFeignClient;

    @Override
    public UserDTO queryByLoginName(String loginName) {

        ServiceResult<UserDTO> serviceResult = userFeignClient.queryByLoginName(loginName);

        ServiceChecker.checkAndAssert(serviceResult);

        return serviceResult.getResultObj();
    }

    @Override
    public List<RoleDTO> queryRolesByUserId(Integer userId) {

        ServiceResult<List<RoleDTO>> serviceResult = userFeignClient.queryRolesByUserId(userId);

        ServiceChecker.checkAndAssert(serviceResult);

        return serviceResult.getResultObj();
    }

    @Override
    public List<PermssionDTO> queryPermsByRoleIds(List<Integer> roleIds) {

        ServiceResult<List<PermssionDTO>> serviceResult = userFeignClient.queryPermsByRoleIds(roleIds);

        ServiceChecker.checkAndAssert(serviceResult);

        return serviceResult.getResultObj();
    }
}
