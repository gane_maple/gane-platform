package com.gane.auth.manager.impl;

import com.gane.auth.dto.CurrentUser;
import com.gane.auth.manager.LoginManager;
import com.gane.common.template.result.ServiceResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Date 2020/4/27 15:17
 * @Created by 王弘博
 */
@Slf4j
@Service
public class LoginManagerImpl implements LoginManager {

    /*@Override
    public ServiceResult<CurrentUser> login(String loginName, String password) {
        return null;
    }

    @Override
    public ServiceResult logout() {
        return null;
    }

    @Override
    public ServiceResult<CurrentUser> currentUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Object details = authentication.getDetails();

        return null;
    }*/

    /*@Resource
    private HttpServletRequest httpServletRequest;

    @Override
    public ServiceResult<CurrentUser> login(String loginName, String password) {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback<CurrentUser>() {

            @Override
            public void beforeService() {

                AssertUtil.isTrue(!StringUtils.isAnyBlank(loginName, password), BizErrorCode.PARAM_ILLEGA);
            }

            @Override
            public ServiceResult<CurrentUser> executeService() {

                Subject subject = SecurityUtils.getSubject();

                UsernamePasswordToken token = new UsernamePasswordToken(loginName, password);

                subject.login(token);

                if (!subject.isAuthenticated()) {
                    token.clear();
                    return ServiceResult.valueOfFail(BizErrorCode.USERNAME_OR_PASSWORD_ERROR);
                }

                CurrentUser shiroUser = (CurrentUser) subject.getPrincipal();

                return ServiceResult.valueOfSuccess(shiroUser);

            }
        });

    }

    @Override
    public ServiceResult logout() {

        return ServiceTemplateImpl.serviceProcess(new ServiceCallback() {

            @Override
            public ServiceResult executeService() {

                Subject subject = SecurityUtils.getSubject();

                if (subject.isAuthenticated()) {
                    subject.logout();
                }

                return ServiceResult.valueOfSuccess(true);

            }
        });
    }*/
}
